
# tethercode - unit test
# Copyright (c) 2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import json

func _test_ok(data, strict_mode=true) {
    let result = json.parse(data, strict_mode=strict_mode)
}

func _test_fail(data, strict_mode=true) {
    try {
        let result = json.parse(data,
            strict_mode=strict_mode)
    } catch ValueException as e {
        return
    }
    throw RuntimeException(
        "_test_fail on JSON data unexpectedly succeeded " +
        "with strict_mode=" + tostring(strict_mode) +
        " with test data: " + tostring(data))
}

func test_json {
    let result = json.parse('{"a":5, "b":[1, 2]}',
        debug=false)
    assert(type(result) == BaseType.dictionary)
    assert(result["a"] == 5)
    assert(result["b"].length == 2)
    
    _test_ok("[1, 2, 3]")
    _test_ok("{\"a\":\"b\"}", strict_mode=true)
    _test_fail("{\"a\":1,'b':2}", strict_mode=true)
    _test_ok("{\"a\":1,'b':2}", strict_mode=false)
}


func test_json_rpc {
    # This test is from real-world examples from LSP JSON RPC with
    # atom-languageclient.
    let result = json.parse(
        '{"jsonrpc":"2.0","id":0,"method":"initialize",' +
        '"params":{"processId":8044,"capabilities":{},' +
        '"rootPath":"/home/jonas/Develop/tethercode-language-server"}}',
        debug=false)
    assert(type(result) == BaseType.dictionary)
    result = json.parse(
        '{"jsonrpc":"2.0","id":1,"method":"shutdown","params":null}',
        debug=false)
    assert(type(result) == BaseType.dictionary)
    result = json.parse(
        '{"jsonrpc":"2.0","id":0,"method":"initialize",' +
        '"params":{"processId":23004,"capabilities":{},' +
        '"rootPath":"/home/jonas/Develop/tethercode-stdlib"}}',
        debug=false)
    assert(type(result) == BaseType.dictionary and
        type(result["params"]) == BaseType.dictionary and
        "rootPath" in result["params"] and
        type(result["params"]["rootPath"]) == BaseType.string)
    result = json.parse(
        '{"jsonrpc":"2.0","method":"textDocument/didChange","params":{"textDocument":{"uri":"file:///home/jonas/Develop/tethercode-language-server/clientmodel.tc","version":26},"contentChanges":[{"range":{"start":{"line":278,"character":17},"end":{"line":278,"character":17}},"rangeLength":0,"text":"\\""}]}}',
        debug=false)
}

func test_dump {
    # Dump an object and parse it again, to see if we get something similar:
    let result = json.dump({"a":{}, "b":[1,2, 3], "c":{"a":"b", "d":[]}})
    let restored = json.parse(result)
    assert(type(restored) == BaseType.dictionary)
    assert("a" in restored and "b" in restored and "c" in restored)
    assert(type(restored["c"]) == BaseType.dictionary)
    assert("a" in restored["c"] and "d" in restored["c"])
    assert(restored["b"].length == 3)
}
