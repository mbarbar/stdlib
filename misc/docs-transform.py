#!/usr/bin/python3

'''
Documentation Link Transformer for the tethercode documentation
Copyright (c) 2016, Jonas Thiem et al. (see AUTHORS.md)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

SPDX-License-Identifier: Zlib

'''

"""
This link transformer will find all links in a markdown document which point
to files ending in .md, and replace them with links pointing to the same file
but ending in .html instead. This is required e.g. when using pandoc's html
backend to get working inter-document links.

To use this tool, simply provide a markdown file path as a command line
argument and the tool will open the file, read it, transform it and output
the transformed result directly to stdout.
"""

import sys

file_path = sys.argv[1]

with open(file_path, "rb") as f:
    contents = f.read()

# Transform links:
i = 0
while i < len(contents):
    # Find link if any:
    if contents[i:].startswith(b"]("):
        end_index = contents[i:i+50].find(b".md)")
        if end_index < 0:
            i += 1
            continue
    else:
        i += 1
        continue
    # Transform link, unless it points outside:
    if contents[i+2:].lstrip().startswith(b"../"):
        i += 1
        continue
    new_contents = contents[:i + end_index]
    new_contents += b".html)" + contents[i + end_index + len(".md)"):]
    contents = new_contents
    i += 1

# Transform section headers to remove first (since top bar covers it):
new_lines = []
removed_first = False
inside_code = False
def count_code_quotations(s):
    count = 0
    i = 0
    while i < len(s):
        if s[i:].startswith(b"```"):
            count += 1
        i += 1
    return count
for line in contents.replace(b"\r\n", b"\n").split(b"\n"):
    if line.startswith(b"# ") and not removed_first:
        removed_first = True
        continue
    n = count_code_quotations(line)
    if (n % 2) != 0:
        inside_code = not inside_code
    if not inside_code:
        if line.startswith(b"##"):
            line = line[1:]
    new_lines.append(line)
contents = b"\n".join(new_lines)

sys.stdout.buffer.write(contents)
sys.stdout.buffer.flush()

