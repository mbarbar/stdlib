" Vim syntax file
" Language: tethercode
" Maintainer: Jonas Thiem
" Latest Revision: 21 March 2017

if exists("b:current_syntax")
  finish
endif

" Strings
syntax region TetherasmString start=+\v\"+ skip=+\M\\\\\|\\"+ end=+\v\"+
syntax region TetherasmString start=+\v'+ skip=+\M\\\\\|\\'+ end=+\v'+

" Keywords
syntax keyword TetherasmBasicLanguageKeywords FUNCDEF LABEL LOADBUILTIN NEW COPYTOP CALLVAR END
syntax keyword TetherasmTypeKeywords NUMLITERAL STRINGLITERAL BYTESLITERAL NULLLITERAL
syntax keyword TetherasmSpecialLiteral true false null
syntax cluster TetherasmKeywords contains=TetherasmBasicLanguageKeywords,TetherasmIdentifier,TetherasmSpecialLiteral

" Numbers
syntax match TetherasmVarLabel "\v(:)@![a-zA-Z_][0-9a-zA-Z_]+"
syntax match TetherasmNumber "\v-?[0-9]+|0x[0-9a-fA-F]+"

" Storage identifiers
syntax match TetherasmIdentifier "\vt[0-9]+"

" Special attributes
syntax match TetherasmAttribute "\v:(const|unmanaged)([a-zA-Z0-9_])@!"

" Comments
syntax match TetherasmLineComment ";.*" contains=@spell

let b:current_syntax = "tetherasm"

hi def link TetherasmBasicLanguageKeywords Keyword
hi def link TetherasmString String
hi def link TetherasmIdentifier Identifier
hi def link TetherasmTypeKeywords Special
hi def link TetherasmLineComment Comment
hi def link TetherasmSpecialLiteral Boolean
hi def link TetherasmNumber Number

