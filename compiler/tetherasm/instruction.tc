
# tethercode compiler library - tetherasm/instruction.tc
# Copyright (c) 2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import encoding
import json
import url

import compiler.location
import compiler.tetherasm.instructiontable
import compiler.tokenizer

## A generic representation for a tetherasm instruction (or an intermediate
## tetherasm instruction)
class Instruction {
    ## Op code of the instruction
    let op_code

    let arg1
    let arg2
    let arg3
    let arg4

    ## During tethercode to tetherasm generation, this refers to the original
    ## compiler.grammar.expression.Expression this was generated for.
    ## (This information is thrown away once the final tetherasm code is
    ## serialized to text)
    let expr

    ## The location in the original tethercode source which this instruction
    ## is generated for
    let location

    ## The location of this instruction in the generated tetherasm code
    let asm_location

    ## Special hack to ensure NEW x STRINGLITERAL "value" always has a quoted
    ## value
    let is_new_string_literal

    func init(op_code, arg1=null, arg2=null, arg3=null,
            arg4=null,
            expr=null,
            location=null) {
        self.op_code = op_code
        self.arg1 = arg1
        self.arg2 = arg2
        self.arg3 = arg3
        self.arg4 = arg4
        self.expr = expr
        if expr != null and expr.start_location != null {
            self.location = expr.start_location
        } elseif location != null {
            self.location = location
        }

        # Special handling for string literals:
        if self.op_code.upper() == "NEW" and self.arg2 != null and
                self.arg2.upper() == "STRINGLITERAL" {
            self.is_new_string_literal = true
        }
    }

    func escape_arg(arg, force_quotes=false) {
        # Don't escape nested instructions:
        if type(arg) == Instruction {
            return arg.to_tetherasm(debug_annotation=false).trim()
        } elseif type(arg) == Ref {
            return tostring(arg)
        }

        # Convert to bytes string:
        let t = arg
        let needs_escaping = false
        if type(arg) != BaseType.bytes {
            t = tostring(arg)
            if type(arg) == url.Url {
                t = arg.urlstr()
            }
            t = encoding.encode(t)
        }

        # Check if any character requires quote-escaping the value,
        # and backslash-escape some characters as necessary:
        let i = 0
        let transformed = b""
        while i < t.length {
            let c = t[i]
            preccheck_if translator_name == "baseline"
            if type(c) != BaseType.bytes {
                let newc = b"0"
                newc[0] = c
                c = newc
            }
            preccheck_end

            if (ord(c) < ord(b"a") or
                    ord(c) > ord(b"z")) and
                    (ord(c) < ord(b"A") or
                    ord(c) > ord(b"z")) and
                    (ord(c) < ord(b"0") or
                    ord(c) > ord(b"9")) and
                    c != b"_" and c != b"." {
                needs_escaping = true
            }
            if ord(t[i]) < 32 or ord(t[i]) > 127 {
                let v = encoding.encode(
                    encoding.number_to_hex(
                    ord(t[i])))
                if v.length < 2 {
                    v = b"0" + v
                }
                transformed += b"\\x" + v
            } else {
                transformed += c
            }
            i += 1
        }
        t = encoding.decode(t)

        # Return result with escaping if necessary:
        if needs_escaping or force_quotes {
            return "\"" + t.replace("\\", "\\\\").replace(
                "\"", "\\\"") + "\""
        }
        return t
    }

    func to_tetherasm(debug_annotation=true) {
        let t = self.op_code
        if self.arg1 != null {
            t += " " + self.escape_arg(self.arg1)
        }
        if self.arg2 != null {
            t += " " + self.escape_arg(self.arg2)
        }
        if self.arg3 != null {
            if self.is_new_string_literal {
                t += " " + self.escape_arg(self.arg3,
                    force_quotes=true)
            } else {
                t += " " + self.escape_arg(self.arg3)
            }
        }
        if self.arg4 != null {
            t += " " + self.escape_arg(self.arg4)
        }
        if debug_annotation {
            t += "    ; location="
            if self.location != null {
                t += json.dump(self.location.to_json_object())
            } else {
                t += "null"
            }
        }
        return t
    }

    func _arg_print(a) {
        if a == null {
            return ""
        }
        if isinstanceof(a, InstructionArg) {
            return " " + tostring(a)
        } else {
            return " \"" + self.escape_arg(a) + "\""
        }
    }

    func tostring {
        let t = "" + self._arg_print(self.arg1) +
            self._arg_print(self.arg2) +
            self._arg_print(self.arg3) +
            self._arg_print(self.arg4)
        if self.location != null {
            t += " \"; location="
            t += json.dump(self.location.to_json_object())
            t += "\""
        } else {
            t += " <unknown location>"
        }
        return 'Instruction("' + self.op_code + '"' + t + ')'
    }
}

class InstructionParseResult {
    let errors = []
    let warnings = []
    let result = null
}

class InstructionArg {
}

class InstructionArgTemp extends InstructionArg {
    let id

    func init(i) {
        self.id = i
    }

    func tostring {
        return "InstructionArgTemp(" + self.id + ")"
    }
}

class InstructionArgGlobal extends InstructionArg {
    let id

    func init(i) {
        self.id = i
    }

    func tostring {
        return "InstructionArgGlobal(" + self.id + ")"
    }
}

class InstructionArgLabel extends InstructionArg {
    let label

    func init(l) {
        self.label = l
    }

    func tostring {
        return "InstructionArgLabel(\"" + self.label + "\")"
    }
}

class InstructionArgLiteral extends InstructionArg {
    let value

    func init(v) {
        self.value = v
    }

    func tostring {
        return "InstructionArgTemp('" + tostring(self.value) + "')"
    }
}

let instruction_table = null
func on_import {
    try {
        instruction_table = compiler.tetherasm.instructiontable.
            InstructionTable()
        assert(instruction_table != null)
    } catch Exception as e {
        print("compiler/tetherasm/instructiontable.tc on_import " +
            "ERROR: " + tostring(e), stderr=true)
    }
}

func parse_instruction(
        code_line,
        line_no=-1,
        source_file_name=b"") {
    let result = InstructionParseResult()
    let line = code_line

    let column = 1

    # If already an instruction, return it:
    if type(line) == Instruction {
        result.result = line
        return result
    }

    # Convert to unicode string:
    if type(line) == BaseType.bytes {
        line = encoding.decode(line)
    }

    # Skip over whitespace:
    let i = 0
    while i < line.length {
        if line[i] == " " or line[i] == "\t" {
            i += 1
            column += 1
        } else {
            break
        }
    }
    line = line[i:]

    # Stop here if it's an empty line or a comment:
    if line.length == 0 or line[0] == ";" {
        return result
    }

    # See if this starts a valid instruction name:
    let firstc = line[0]
    if (ord(firstc) < ord("a") or ord(firstc) > ord("a")) and
            (ord(firstc) < ord("A") or ord(firstc) > ord("Z")) {
        result.errors.add({
            "text" : "unexpected character " +
                compiler.tokenizer.print_char(firstc) +
                " - expected tetherasm instruction instead",
            "label" : "syntax",
            "location" : compiler.location.Location(
                line_no, column, source=source_file_name),
        })
        return result
    }

    # Extract instruction name:
    i = 0
    while i < line.length {
        let c = line[i]
        if (ord(c) < ord("a") or ord(c) > ord("a")) and
                (ord(c) < ord("A") or ord(c) > ord("Z")) and
                (ord(c) < ord("0") or ord(c) > ord("9")) and
                c != "_" {
            break
        }
        i += 1
        column += 1
    }

    # Create instruction object:
    let instruction_name = line[:i].upper()
    line = line[i:]
    result.result = Instruction(instruction_name)
    let current_arg = 0
    result.result.asm_location = compiler.location.Location(
        line_no, column, source=source_file_name)

    # Look up instruction info:
    if not instruction_name in instruction_table.table {
        result.errors.add({
            "text" : "unknown instruction '" +
                tostring(instruction_name) + "'",
            "label" : "semantics",
            "location" : compiler.location.Location(
                line_no, column, source=source_file_name),
        })
        return result
    }
    let iinfo = instruction_table.table[instruction_name]

    # Parse all arguments:
    while line.length > 0 {
        current_arg += 1

        func add_arg(v) {
            if current_arg == 1 {
                result.result.arg1 = v
            } elseif current_arg == 2 {
                result.result.arg2 = v
            } elseif current_arg == 3 {
                result.result.arg3 = v
            } elseif current_arg == 4 {
                result.result.arg4 = v
            }
        }

        func parameter_is_label {
            if current_arg - 1 >= iinfo.args.length {
                return false
            }
            let arg_info = iinfo.args[current_arg - 1]
            if arg_info.type == "label" {
                return true
            }
            return false
        }

        func parameter_is_any {
            if current_arg - 1 >= iinfo.args.length {
                return false
            }
            let arg_info = iinfo.args[current_arg - 1]
            if arg_info.type == "any" {
                return true
            }
            return false
        }

        # Make sure there is no invalid character following:
        if line[0] != " " and line[0] != "\t" and
                line[0] != ";" {
            result.errors.add({
                "text" : "unexpected character " +
                    compiler.tokenizer.print_char(line[0]) +
                    " - expected whitespace, ';' or end of line instead",
                "label" : "syntax",
                "location" : compiler.location.Location(
                    line_no, column, source=source_file_name),
            })
            return result
        }

        # Process end of instruction if we reached it:
        while line.length > 0 and (line[0] == " " or
                line[0] == "\t" or line[0] == "\n" or
                line[0] == "\t") {
            column += 1
            line = line[1:]
        }
        if line.length == 0 {
            break
        }
        if line[0] == ";" {
            # Extract debug location info if any:
            let start_index = -1
            let end_index = -1
            let i = 0
            while i < line.length {
                if line[i:].startswith("location={") {
                    start_index = i + "location=".length
                } elseif start_index >= 0 and line[i] == "}" {
                    end_index = i
                    break
                }
                i += 1
            }

            # Parse extracted debug location info to location object:
            if start_index >= 0 and end_index > start_index {
                let location_obj = line[start_index:end_index + 1]
                line = line[end_index + 1:]
                column += end_index + 1
                let parsed_obj
                let parsed_location
                try {
                    parsed_obj = json.parse(location_obj)
                    if parsed_obj != null {
                        parsed_location = compiler.location.
                            create_location_from_json_object(
                            parsed_obj)
                    }
                } catch json.JSONParseException as e {
                    result.warnings.add({
                        "text" : "unparseable debug location info ("
                            + "JSON syntax error)",
                        "label" : "unparseable_location_info",
                        "location" : compiler.location.Location(
                            line_no, column, source=source_file_name),
                    })
                }
                if parsed_location != null {
                    result.result.location = parsed_location
                }
            }

            # This is a line comment, so nothing else of interest follows.
            break
        }

        # If we already have 4 args, stop with error:
        if current_arg > 4 {
            result.errors.add({
                "text" : "unexpected character " +
                    compiler.tokenizer.print_char(line[0]) +
                    " - expected end of instruction since " +
                    "the maximum of 4 arguments was already reached",
                "label" : "syntax",
                "location" : compiler.location.Location(
                    line_no, column, source=source_file_name),
            })
            return result
        } 

        # Extract argument:
        let param_is_label = parameter_is_label()
        let param_is_any = parameter_is_any()
        if line.startswith("t") and
                line.length > 1 and
                ord(line[1]) >= ord("0") and
                ord(line[1]) <= ord("9") and
                not param_is_label {
            column += 1
            i = 1
            while i < line.length {
                if ord(line[i]) >= ord("0") and
                        ord(line[i]) <= ord("9") {
                    column += 1
                    i += 1
                    continue
                }
                break
            }
            let temp_num = tonumber(line[1:i])
            line = line[i:]
            add_arg(InstructionArgTemp(temp_num))
        } elseif not param_is_label and
                line.startswith("g") and
                line.length > 1 and
                ord(line[1]) >= ord("0") and
                ord(line[1]) <= ord("9") {
            column += 1
            i = 1
            while i < line.length {
                if ord(line[i]) >= ord("0") and
                        ord(line[i]) <= ord("9") {
                    column += 1
                    i += 1
                    continue
                }
                break
            }
            let global_num = tonumber(line[1:i])
            line = line[i:]
            add_arg(InstructionArgGlobal(global_num))
        } elseif not param_is_label and (line.length > 0 and
                (line[0] == "'" or line[0] == '"' or
                line.startswith("b'") or
                line.startswith('b"'))) {
            let start_column = column
            column += 1
            let quotation = line[0]
            if line[0] == "b" {
                quotation = line[1]
            }
            let end_index = 1
            let escaped = false
            while end_index < line.length {
                let c = line[end_index]
                if not escaped {
                    if c == "\\" {
                        quoted = true
                    } elseif c == quotation {
                        break
                    }
                } else {
                    quoted = false
                }
                column += 1
                end_index += 1
            }
            if end_index >= line.length {
                let end_description = "'\"'"
                if quotation == "'" {
                    end_description = '"\'"'
                }
                let what = "string literal"
                if line[0] == "b" {
                    what = "byte literal"
                }
                result.errors.add({
                    "text" : "unexpected end of line in " +
                        what + " starting in line " +
                        line_no + ", column " + start_column +
                        " - expected " + end_description +
                        " instead (please note a multiline " +
                        what + " in tetherasm must use an escaped " +
                        "line break \n unlike " +
                        "in tethercode which allows actual inline " +
                        "line breaks in literals)",
                    "label" : "syntax",
                    "location" : compiler.location.Location(
                        line_no, column, source=source_file_name),
                })
                return result
            }
            column += end_index + 1
            let value
            try {
                value = encoding.parse_literal(line[:end_index+1])
            } catch ValueException as e {
                let what = "string literal"
                if line[0] == "b" {
                    what = "byte literal"
                }
                result.errors.add({
                    "text" : "unexpected invalid " + what +
                        " - " + tostring(e.message),
                    "label" : "syntax",
                    "location" : compiler.location.Location(
                        line_no, start_column, source=source_file_name),
                })
                return result
            }
            line = line[end_index + 1:]
            add_arg(InstructionArgLiteral(
                value))
        } elseif line.length > 0 and not param_is_label and
                (ord(line[0]) >= ord("0") and
                ord(line[0]) <= ord("9")) {
            let start_column = column
            let i = 0
            while i < line.length and (
                    (ord(line[i]) >= ord("0") and
                    ord(line[i]) <= ord("9")) or
                    (ord(line[i]) >= ord("a") and
                    ord(line[i]) <= ord("f")) or
                    (ord(line[i]) >= ord("A") and
                    ord(line[i]) <= ord("F")) or
                    line[i] == "x" or line[i] == "X" or
                    line[i] == ".") {
                column += 1
                i += 1
            }
            let value
            try {
                value = encoding.parse_literal(line[:i])
            } catch ValueException as e {
                result.errors.add({
                    "text" : "unexpected invalid number literal " +
                        " - " + tostring(e.message),
                    "label" : "syntax",
                    "location" : compiler.location.Location(
                        line_no, start_column, source=source_file_name),
                })
                return result
            }
            line = line[i:]
            add_arg(InstructionArgLiteral(
                value))
        } elseif not param_is_label and 
                (line.startswith("true ") or line.startswith("true;")
                or line.startswith("false ") or line.startswith("false")
                or line == "true" or line == "false"
                or line.startswith("null ") or line.startswith("null;")
                or line == "null") {
            let value = false
            if line.startswith("true") {
                column += "true".length
                line = line["true".length:]
                value = true
            } elseif line.startswith("null") {
                column += "null".length
                line = line["null".length:]
                value = null
            } else {
                column += "false".length
                line = line["false".length:]
                value = false
            }
            add_arg(InstructionArgLiteral(value))
        } elseif line.length > 0 and (
                (ord(line[0]) >= ord("a") and
                ord(line[0]) <= ord("z")) or
                (ord(line[0]) >= ord("A") and
                ord(line[0]) <= ord("Z")) or
                line[0] == "_" or line[0] == "$") {
            if not param_is_label and not param_is_any {
                result.errors.add({
                    "text" : "unexpected label parameter " +
                        "starting with " +
                        compiler.tokenizer.print_char(line[0]) +
                        " - instruction " + instruction_name +
                        " doesn't take a label as parameter #" +
                        current_arg,
                    "label" : "semantics",
                    "location" : compiler.location.Location(
                        line_no, column, source=source_file_name),
                })
                return result
            }
            let end_index = 0
            while i < line.length and (
                    (ord(line[i]) >= ord("a") and
                    ord(line[i]) <= ord("z")) or
                    (ord(line[i]) >= ord("A") and
                    ord(line[i]) <= ord("Z")) or
                    (ord(line[i]) >= ord("0") and
                    ord(line[i]) <= ord("9")) or
                    line[i] == "_" or line[0] == "$") {
                column += 1
                i += 1
            }
            let value = line[:i]
            line = line[i:]
            add_arg(InstructionArgLabel(value))
        } else {
            result.errors.add({
                "text" : "unexpected character " +
                    compiler.tokenizer.print_char(line[0]) +
                    " - expected a valid instruction argument",
                "label" : "syntax",
                "location" : compiler.location.Location(
                    line_no, column, source=source_file_name),
            })
            return result
        }
    }

    if current_arg - 1 != iinfo.args.length {
        result.errors.add({
            "text" : "mismatched argument count for " +
                "instruction " + result.result.op_code +
                " - expected " + iinfo.args.length +
                " arguments, got " +
                (current_arg - 1) + " arguments",
            "label" : "semantics",
            "location" : compiler.location.Location(
                line_no, column, source=source_file_name),
        })
        return result
    }

    return result
}


class Ref {
    let ref
    let instruction_generator
    let write_to_target_temporary

    func init(instruction_generator,
            from_value=null, from_dict=null, from_dict_key=null,
            preferred_target_temporary=null) {
        self.instruction_generator = instruction_generator

        # If we got passed a preferred target temporary to store
        # the result in, extract it:
        if preferred_target_temporary != null {
            if type(preferred_target_temporary) == Ref {
                self.write_to_target_temporary =
                    preferred_target_temporary.tmp_id()
            } elseif type(preferred_target_temporary) ==
                    compiler.grammar.instruction.Instruction and
                    preferred_target_temporary.op_code == "TEMP" {
                let value = preferred_target_temporary.op_code
                if value.startswith("t") {
                    value = value[1:]
                }
                self.write_to_target_temporary = tonumber(value)
            } else {
                throw ValueException(
                    "invalid preferred_target_temporary value which " +
                    "doesn't seem to be any sort of temporary")
            }
        }       
 
        # Obtain the value we should make a reference to, and
        # process it properly:
        let obtained_from_value = null
        if from_value != null {
            if from_dict != null or from_dict_key != null {
                throw ValueException(
                    "invalid Ref() constructor parameters: " +
                    "from_value set but from_dict/from_dict_key " +
                    "is also set. You may only specify one " +
                    "or the other")
            }
            obtained_from_value = from_value
            self._set_from_value(from_value)
        } elseif from_dict != null {
            if from_dict_key in from_dict {
                obtained_from_value = from_dict[from_dict_key]
                self._set_from_value(from_dict[from_dict_key])
            } else {
                self.ref = Instruction("UNKNOWNREF")
            }
        } else {
            throw ValueException(
                "invalid Ref() constructor parameters: " +
                "no from_value set and no from_dict/from_dict_key " +
                "specified either - what is this Ref supposed to " +
                "refer to??")
        }
        
        # This should never happen, so add an assertion to
        # catch it during testing:
        assert(type(self.ref) != Instruction or
            self.ref.op_code != "TEMP" or
            type(self.ref.arg1) != Ref,
            "a Ref must never contain a TEMP instruction " +
            "again being a Ref - but this happened somehow " +
            "anyway. Original value this Ref was constructed " +
            "from: " + tostring(obtained_from_value))
        assert(self.ref != null,
            "a Ref must never end up with null contents, " +
            "but still we somehow did. Original value " +
            "this Ref was constructed from: " +
            tostring(obtained_from_value))
    }

    ## Return a temporary var id which represents the value referenced
    ## by this Ref() instance. This doesn't work directly for globals - use
    ## force_convert_to_temporary() to force a conversion to a temporary var
    ## if you need to convert a global reference to one with a temporary var
    ## id.
    func tmp_id {
        if self.ref.op_code == "TEMP" {
            let val = self.ref.arg1
            if type(self.ref.arg1) == BaseType.string {
                if self.ref.arg1.startswith("t") {
                    val = self.ref.arg1[1:]
                }
            }
            assert(type(val) != Instruction and
                type(val) != Ref)
            return tonumber(val)
        }
        throw ValueException(
            "cannot implicitly use this Ref to temporary - " +
            "use force_convert_to_temporary() if you want " +
            "to convert it irrevocably")
    }

    func force_convert_to_temporary {
        if self.ref.op_code == "GLOBAL" {
            # Obtain global id we're referencing:
            let val = self.ref.arg1
            if type(self.ref.arg1) == BaseType.string {
                if self.ref.arg1.startswith("g") {
                    val = self.ref.arg1[1:]
                }
            }

            # Get a suitable target temporary:
            let tmp = self.write_to_target_temporary
            if tmp == null {
                tmp = self.instruction_generator.gen_temp(value.expr)
            }

            # Add instruction to assign to temporary:
            self.instruction_generator.add_inst(
                Instruction("SETVALUE",
                    Ref(self.instruction_generator, "t" + tmp),
                    value.arg1,
                    expr=value.expr),
                    Ref(self.instruction_generator, "g" + val))
            self.ref = Instruction("TEMP",
                "t" + tmp)
            return
        } elseif self.ref.op_code == "TEMP" {
            return
        } else {
            throw RuntimeException("conversion not implemented for " +
                "this reference type")
        }
    }

    ## Check whether this Ref() can be directly used as a temporary with
    ## tmp_id() returning the according temporary number.
    func is_tmp {
        try {
            let v = self.tmp_id()
            return true
        } catch ValueException {
            return false
        }
    }
 
    func tostring {
        if self.ref.op_code == "TEMP" {
            let val = self.ref.arg1
            if type(self.ref.arg1) == BaseType.string {
                if self.ref.arg1.startswith("t") {
                    val = self.ref.arg1[1:]
                }
            }
            assert(type(val) != Instruction and
                type(val) != Ref)
            return "t" + tonumber(val)
        } elseif self.ref.op_code == "GLOBAL" {
            let val = self.ref.arg1
            if type(self.ref.arg1) == BaseType.string {
                if self.ref.arg1.startswith("g") {
                    val = self.ref.arg1[1:]
                }
            }
            assert(type(val) != Instruction and
                type(val) != Ref)
            return "g" + tonumber(val)
        } elseif self.ref.op_code == "UNKNOWNREF" {
            return "UNKNOWNREF"
        }
        let op_code_text = ""
        if type(self.ref) == Instruction {
            op_code_text = ", op_code: " + self.ref.op_code
        }
        throw RuntimeException(
            "cannot convert this Ref to a string " +
            "with self.ref: " + tostring(self.ref) +
            " (type: " + tostring(type(self.ref)) +
            op_code_text + ")")
    }

    func _set_from_value(value) {
        if type(value) == Instruction and
                value.op_code == "UNKNOWNREF" {
            self.ref = value
            return
        }

        if type(value) == Ref {
            self.ref = value.ref
            return
        }

        if type(value) == BaseType.number {
            self.ref = Instruction("TEMP",
                value)
            return
        }

        if type(value) == BaseType.string and
                value.startswith("t") and
                value.length > 1 and
                ord(value[1]) >= ord("0") and
                ord(value[1]) <= ord("9") {
            self.ref = Instruction("TEMP",
                value)
            return
        }

        if type(value) == BaseType.string and
                value.startswith("g") and
                value.length > 1 and
                ord(value[1]) >= ord("0") and
                ord(value[1]) <= ord("9") {
            self.ref = Instruction("GLOBAL",
                value)
            return
        }

        if type(value) == BaseType.string {
            throw RuntimeException(
                "unparseable string submitted as Ref: " +
                value)
        }

        if (value.op_code.startswith("t") or
                value.op_code.startswith("g")) and
                value.op_code.length > 1 and
                ord(value.op_code[1]) >= ord("0") and
                ord(value.op_code[1]) <= ord("9") {
            self._set_from_value(value.op_code)
            return
        }

        if value.op_code == "TEMP" {
            let val = value.arg1
            
            # TEMP with Ref arg:
            if type(val) == Ref {
                self.ref = val.ref
                # Make sure this isn't a more deeply nested
                # TEMP chain:
                if type(self.ref) == Instruction and
                        self.ref.op_code == "TEMP" {
                    self._set_from_value(self.ref)
                }
                return
            }
            
            # TEMP with number/string arg:
            if type(val) == BaseType.string {
                if val.startswith("t") {
                    val = value.arg1[1:]
                }
            }
            self.ref = Instruction("TEMP",
                "t" + tonumber(val))
            return
        }
        if value.op_code == "GLOBAL" or value.op_code == "FUNCDEFREF"
                or value.op_code == "CLASSDEFREF"
                or value.op_code == "CLOSURECELL" {
            if value.expr == null {
                throw RuntimeException("invalid " + value.op_code +
                    " instruction " +
                    "with empty expression: " + tostring(value))
            }
            let tmp = self.write_to_target_temporary
            if tmp == null {
                tmp = self.instruction_generator.gen_temp(value.expr)
            }

            let access_opcode = "LOADGLOBAL"
            if value.op_code != "GLOBAL" {
                access_opcode = "LOAD" + value.op_code
            }
            self.instruction_generator.add_inst(
                Instruction(access_opcode, tmp,
                    value.arg1, expr=value.expr))
            self.ref = Instruction("TEMP",
                "t" + tmp, expr=value.expr)
            return
        }
        if value.op_code == "EXTGLOBAL" {
            if value.expr == null {
                throw RuntimeException("invalid EXTGLOBAL instruction " +
                    "with empty expression: " + tostring(value))
            }
            let tmp = self.write_to_target_temporary
            if tmp == null {
                tmp = self.instruction_generator.gen_temp(value.expr)
            }
            self.instruction_generator.add_inst(
                Instruction("LOADEXTGLOBALBYREF",
                    Ref(self.instruction_generator, "t" + tmp),
                    value.arg1, value.arg2,
                    expr=value.expr))
            self.ref = Instruction("TEMP",
                "t" + tmp, expr=value.expr)
            return
        }
        if value.op_code == "BUILTIN" {
            if value.expr == null {
                throw RuntimeException("invalid BUILTIN instruction " +
                    "with empty expression: " + tostring(value))
            }
            let tmp = self.write_to_target_temporary
            if tmp == null {
                tmp = self.instruction_generator.gen_temp(value.expr)
            }
            self.instruction_generator.add_inst(
                Instruction("LOADBUILTIN",
                    Ref(self.instruction_generator, "t" + tmp),
                    value.arg1,
                    expr=value.expr))
            self.ref = Instruction("TEMP",
                "t" + tmp)
            return
        }
        if value.op_code == "BINOP_INLINE" {
            if value.expr == null {
                throw RuntimeException(
                    "invalid BINOP_INLINE instruction " +
                    "with empty expression: " + tostring(value))
            }
            let tmp = self.write_to_target_temporary
            if tmp == null {
                tmp = self.instruction_generator.gen_temp(value.expr)
            }
            self.instruction_generator.add_inst(
                Instruction(
                    "BINOP",
                    "t" + tmp,
                    value.arg1,
                    value.arg2,
                    value.arg3,
                    expr=value.expr))
            self.ref = Instruction("TEMP",
                "t" + tmp)
            return
        }
        if value.op_code == "UNOP_INLINE" {
            if value.expr == null {
                throw RuntimeException(
                    "invalid UNOP_INLINE instruction " +
                    "with empty expression: " + tostring(value))
            }
            let tmp = self.write_to_target_temporary
            if tmp == null {
                tmp = self.instruction_generator.gen_temp(value.expr)
            }
            self.instruction_generator.add_inst(
                Instruction(
                    "BINOP",
                    "t" + tmp,
                    value.arg1,
                    value.arg2,
                    expr=value.expr))
            self.ref = Instruction("TEMP",
                "t" + tmp)
            return
        }
        if value.op_code == "BINOP" or value.op_code == "UNOP" {
            if value.expr == null {
                throw RuntimeException(
                    "invalid BINOP instruction " +
                    "with empty expression: " + tostring(value))
            }
            self.ref = Ref(self.instruction_generator, value.arg1)
            return
        }
        if value.op_code == "IMPORTEDEXPRPLACEHOLDER" {
            throw RuntimeException(
                "got invalid IMPORTEDEXPRPLACEHOLDER: " +
                tostring(value) + " (associated expr: " +
                tostring(value.expr) + ")")
        }
        if value.op_code == "NEW" {
            self.instruction_generator.add_inst(value)
            if value.arg1 == null {
                throw ValueException(
                    "attempting to generate Ref() from " +
                    "invalid NEW instruction: result " +
                    "parameter is null. Full instruction: " +
                    tostring(value))
            }
            assert(value.arg1 != null)
            let result_ref = Ref(self.instruction_generator,
                value.arg1)
            self.ref = result_ref.ref
            return
        }
        throw RuntimeException("code path not implemented: " +
            "enforcing TEMP of: " + value.op_code)
    }
}



