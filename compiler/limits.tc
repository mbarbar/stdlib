
# tethercode compiler library - limits.tc
# Copyright (c) 2016-2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

class LimitInfo {
    let compile_size_limit_tokens = -1
    let compile_size_limit_bytes = -1
    let execution_time_limit_instructions = -1
    let execution_time_limit_ms = -1
    let execution_memory_limit_mb = -1

    preccheck_if translator_name == "baseline"
    func copy() {
        let linfo = LimitInfo()
        linfo.compile_size_limit_tokens = self.compile_size_limit_tokens
        linfo.compile_size_limit_bytes = self.compile_size_limit_bytes
        linfo.execution_time_limit_instructions = (
            self.execution_time_limit_instructions)
        linfo.execution_time_limit_ms = self.execution_time_limit_ms
        linfo.execution_memory_limit_mb = self.execution_memory_limit_mb
        return linfo
    }
    preccheck_end

    func has_effective_execution_limits() {
        return (self.execution_time_limit_instructions >= 0 or
            self.execution_time_limit_ms >= 0 or
            self.execution_memory_limit_mb >= 0)
    }
}

