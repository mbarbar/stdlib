
# tethercode compiler library - unit test
# Copyright (c) 2016-2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import io
import path

import compiler.context
import compiler.source

func _test_do(testdata, expected_success=true) {
    let location = precconst_currfilepath
    let dir_location
    let pos = location.rfind(path.separator())
    if pos >= 0 {
        dir_location = location[:pos]
    } else {
        dir_location = ""
    }   

    # Compute project root and our path seen from project root:
    let project_root_path = path.normalize(path.abspath(
        path.join(dir_location, "..")))
    let file_path = path.join(path.dirname(path.normalize(
        path.abspath(location))),
        ".temp-test-global-scope-test-data.tc")
    try {
        let f = io.open(file_path, "w")
        f.write(testdata)
        f.close()

        # Create compile context:
        let context = compiler.context.create_from_location(
            project_root_path,
            limit_info=null)

        # Compile source and see if it works as expected:
        let source = compiler.source.Source(
            context, file_path,
            source_name=file_path,
            verbose=false)

        let result = source.global_scope()
        if expected_success {
            assert(result != null and result["errors"].length == 0,
                "test data should have been accepted, got error: " +
                tostring(result["errors"][0]))
        } else {
            assert(result != null and result["errors"].length > 0,
                "test data should have been rejected, but got " +
                "no error")
        }
    } catch Exception as e {
        try {
            io.remove(file_path)
        } catch Exception {
        } 
        throw e
    }
    if path.exists(file_path) {
        io.remove(file_path)
    }
}

func _test_fail(testdata) {
    _test_do(testdata, expected_success=false)
}

func _test_ok(testdata) {
    _test_do(testdata, expected_success=true)
}

func test_fail_import_func_symbol_clash {
    _test_fail('
        import encoding

        func encoding {
            print("Hello world!")
        }
        func main {
            encoding()
        }
    ')
}
func test_fail_multiple_global_funcs_clash {
    _test_fail('
        func a {
        }
        func a {
        }
        func main {
            a()
        }
    ')
}
func test_ok_shadow_import_with_local_var {
    _test_ok('
        import encoding
        func main {
            let encoding = "abc"
        }
    ')
}
func test_ok_shadow_global_func_with_local_var {
    _test_ok('
        func encoding {
        }
        func main {
            let encoding = "abc"
        }
    ')
}

func test_ok_let_const {
    _test_ok('
    import path

    let:const v1 = "abc"
    let:const v2 = []
    func main {
    }
    ')
}

