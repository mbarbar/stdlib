
# Contributing to tethercode

Thanks for considering to contribute to tethercode!

Before contributing, please keep in mind that harrassment, personal
insults and specifically harrassment based on religion, ethnicity, gender,
nationality or sexual orientation are not considered appropriate.
This includes the IRC chat on freenode (channel: #tethercode ).

## I found a bug / problem!

If something breaks or if something doesn't appear to be working as it was
intended to be, we would appreciate a bug report!
If you are super uncertain whether it is an actual bug in tethercode or whether
it is caused by some mistake in your own code, please ask for assistance on
the mailing list first:
https://lists.wobblylang.org/mailman3/lists/discuss.lists.wobblylang.org/
FIXME: Adapt address

If you are ready to submit a bug report ticket on the bug tracker, read this:

* **Bug report location:** Please report bugs here:
https://gitlab.com/tethercode/stdlib/issues
(you can use your GitHub account to login!)

* **Check for duplicates!** Do a quick search if your bug was already
reported by using the "Filter by name" search box here:
https://gitlab.com/tethercode/stdlib/issues

* **Use a descriptive title:** Try to give your issue a title that is
sufficiently descriptive. "Calling path.dirname with long paths causes a
runtime error" is a good title, "It doesn't work" is
a bad title.

* **Provide clear steps to reproduce the problem:** Try to provide clear
steps of what you were doing, and include the exact error message you
encountered (if any). *Even if it seems obvious to you, it might not
be immediately clear to others what exactly you tried to do.*
Also make sure to include what you expected to happen instead!

* **Include exact version:** Please provide the exact compiler / standard
library version where you are seeing the problem if applicable.
Use this command to get the version in the packaged SDK:
`tc_packages/bin/tethercc --version`
Use this command to get the commit version of git development versions:
`git rev-parse HEAD`

**IMPORTANT NOTE ON SECURITY ISSUES:** If you report security issues,
we ask that you choose to check the "Confidential" checkbox when reporting
them. This will allow us to provide a fix right along with making the issue
public, allowing people to safely upgrade right away when the issue becomes
generally known to everyone (including potential attackers).


## Request enhancements

If the project lacks something you feel should be included, you can start a
feature request here using a ticket:
https://gitlab.com/tethercode/stdlib/issues

If you aren't sure whether it is worth adding and you want feedback first,
ask about opinions on your idea on the mailing list:
https://lists.wobblylang.org/mailman3/lists/discuss.lists.wobblylang.org/
FIXME: adapt link

If you report a feature request ticket, please keep in mind:

* **Include screenshots, mockups and detailed steps** to be as specific
as possible of what you are suggesting to be added. Your feature request
won't be deleted if you don't, but it has a higher chance of being added
if you're giving us a better idea of what you're after.

* **Please ponder whether a feature is in the scope of the project.**
E.g. a compiler doesn't need amazing 3d graphics. If you are uncertain,
please ask the mailing list for an opinion:
https://lists.wobblylang.org/mailman3/lists/discuss.lists.wobblylang.org/
FIXME: adapt link
This should never stop you from submitting feature requests if you really
think something is worth adding, but adhering to this will make it more
likely that your feature request is considered for inclusion.


## Provide merge requests / new code

To contribute a change directly to tethercode, please submit a merge request
here: https://gitlab.com/tethercode/stdlib/merge_requests

A developer will then incorporate your work as a commit into the official
repository if it is accepted.

Before you even start with a merge request, keep in mind:

* We recommend you **ask around if nobody is working on it to avoid
duplicate work**. This is not required, but it can save you and other
involved people wasted time and frustration.
To do that, we suggest you do a quick search for existing
feature enhancement tickets on [the bug tracker]
    (https://gitlab.com/tethercode/stdlib/issues)
(using the search filter) to find related tickets, and we suggest
you check on their progress and maybe drop a comment about your plans on
implementing it.
Also, consider dropping a note on the [discussion mailing list](
    https://lists.wobblylang.org/mailman3/lists/discuss.lists.wobblylang.org/
). FIXME: adapt link
* **Purely cosmetical code changes (style, refactoring, better comments) are
preferred in smaller separate requests**: split up refactoring in meaningful
small steps. Don't include refactoring of unrelated code with merge requests
implementing new features. If you ignore this, you might be asked to change
your merge request accordingly before it gets merged.


## Forks

If you want to make a fork of the project, the license permits you to do so.
However, if you want to redistribute it beyond just offering it in a git
repository as packaged versions, **you are required to change the logo**
unless it is completely unmodified from this official version.
(the logo is contained in this repo, you should remove/alter it)
Check the [license text](LICENSE.md) for more information and how to apply
for exceptions to this.
We also ask that you change the name in some way to indicate that it isn't
the official version unless you asked us for permission first, at least if
you plan to redistribute your forked version.



