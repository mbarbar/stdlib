
.PHONY: info clean docs clean-docs bootstrap bootstrap-tools-baseline compile check bootstrap

PYTHON3BIN := $(shell command -v python3 2> /dev/null)

all: bootstrap
bootstrap:
ifndef PYTHON3BIN
    $(error "python3 binary missing. Python 3 is required for bootstrapping.")
endif
	rm -rf ./tc_packages/io.tethercode.stdlib/
	rm -rf ./tc_packages/bin/tethercc
	rm -rf ./tc_packages/bin/tcpm
	$(PYTHON3BIN) tools/bootstrap-tcpm.py
	#tc_packages/bin/tethercc --internal-debug-compiler -v tc_packages/io.tethercode.stdlib/compiler/tethercc.tc
debug:
	tools/baseline-runner.py --pretty --node-debugger cicp install chicraftc
	bash -c 'nohup tc_packages/bin/tethercc -v tc_packages/io.tethercode.stdlib/compiler/tethercc.tc > /dev/null 2>&1 &'
	sleep 5
	@bash -c 'node-inspector > /dev/null 2>&1'
debug-file:
	rm -rf ./debug/
	mkdir -p ./tc_packages/cache/
	rm -rf ./tc_packages/cache/io.tethercode.stdlib/
	rm -rf ./tc_packages/io.tethercode.stdlib/
	rm -rf ./tc_packages/bin/chicraftc
	cp -R ./src/io.tethercode.stdlib/ ./tc_packages/cache/io.tethercode.stdlib/
	rm -rf ./tc_packages/cache/io.tethercode.cicp/
	rm -rf ./tc_packages/io.tethercode.cicp/
	rm -rf ./tc_packages/bin/cicp
	cp -R ./src/io.tethercode.cicp/ ./tc_packages/cache/io.tethercode.cicp/
	@tools/baseline-runner.py -v --pretty --packages-dir ./tc_packages/cache/ -o ./debug/ tc_packages/cache/io.tethercode.stdlib/compiler/chicraftc.tc
debug-file-cicp:
	rm -rf ./debug/
	@tools/baseline-runner.py -v --pretty -o ./debug/ tc_packages/cache/io.tethercode.cicp/cicp.tc
debug-file-no-main:
	rm -rf ./debug/
	@tools/baseline-runner.py -v --pretty --no-main -o ./debug tc_packages/io.tethercode.stdlib/compiler/chicraftc.tc
veryclean: clean clean-docs
DOC_FILES_PDF:=$(patsubst %.md,%.pdf,$(wildcard docs/*.md))
DOC_FILES_HTML:=$(patsubst %.md,%.html,$(wildcard docs/*.md))
DOC_FILES=${DOC_FILES_PDF} ${DOC_FILES_HTML}
docs: ${DOC_FILES_HTML}
docs-pdf: ${DOC_FILES_PDF}
docs/%.html: docs/%.md docs/%.md.header docs/docs.css
	misc/docs-transform.py $(basename $@).md | pandoc --number-sections --standalone --smart --toc -c docs.css -T "" -f markdown -t html -B $(basename $@).md.header -A docs/all.footer -o $(basename $@).html
docs/%.pdf: docs/%.md
	cd docs && ../misc/docs-transform.py ../$(basename $@).md | pandoc --number-sections --standalone --smart --toc -T "" -f markdown -o ../$(basename $@).pdf
clean-docs:
	rm -f ${DOC_FILES}
check:
	# Baseline runner tests:
	python3 -m unittest discover -s ./tools/ -p 'test_*.py'
	# Bootstrap wytest:
	$(MAKE) bootstrap-tools-baseline
	# chicraftc and stdlib tests:
	cd src/io.tethercode.stdlib && ../../tc_packages/bin/tctest --baseline ../../tools/baseline-runner.py ./
	
