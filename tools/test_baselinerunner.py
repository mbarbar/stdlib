
'''
ChiCraft Baseline Runner Tests
Copyright (c) 2016, Jonas Thiem et al. (see AUTHORS.md)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

SPDX-License-Identifier: Zlib
'''


import os
import shutil
import subprocess
import sys
import tempfile
import unittest
import uuid

class BaselineTest(unittest.TestCase):
    def setUp(self):
        self.temp_dir = tempfile.mkdtemp(
            prefix="ChiCraft-baseline-unit-tests-")
        self.brunner = os.path.join(
            os.path.abspath(os.path.dirname(__file__)),
            "baseline-runner.py")

    def tearDown(self):
        if hasattr(self, "temp_dir"):
            if self.temp_dir != None:
                if os.path.exists(self.temp_dir):
                    shutil.rmtree(self.temp_dir)

    def _run_code(self, code):
        os.chdir(self.temp_dir)
        code_path = os.path.join(self.temp_dir,
            str(uuid.uuid4()) + ".tc")
        with open(code_path, "w") as f:
            f.write(code)
        result = subprocess.call([
            sys.executable, self.brunner, code_path])
        self.assertEqual(result, 0)

    def test_list(self):
        test_code = """
        func main() {
            let test = [1]
            test.add(2)
            let test2 = []
            test2.add(1)
            test2 = test2[:0]
            test2.add(1)
            assert(test.length == 2)
            assert(test2.length == 1)
        }
        """
        self._run_code(test_code)

    def test_classmemberinit(self):
        test_code = """
        class Test {
            let blih = []
            let blah = []
            func init() {
            }
        }
        func main {
            t1 = Test()
            t1.blah.add(1)
            t2 = Test()
            t2.blih.add(2)
            assert(t1.blih.length == 0)
            assert(t1.blah.length == 1)
            assert(t2.blih.length == 1)
            assert(t2.blah.length == 0)
        }
        """
        self._run_code(test_code)

    def test_dict(self):
        test_code = """
        class TestClass {
            func d() {
                return {
                    "a" : 1,
                    "b" : 2
                }
            }
        }

        func main() {
            let test = TestClass()
            let i = test.d()
            assert("a" in i)
            i.remove("a")
            assert(not "a" in i)
        }
        """
        self._run_code(test_code)

    def test_length(self):
        test_code = """
        class TestClass {
            func full_argument_name(arg) {
                return "full_" + arg
            }

            func run() {
                let arg = "test"
                let shift_len = 18
                let spacing_length = (shift_len -
                    self.full_argument_name (arg).length)
                assert(spacing_length == 9)
            }
        }

        func main() {
            let t = TestClass()
            t.run()
        }
        """
        self._run_code(test_code)

    def test_set(self):
        test_code = """
        func main() {
            test_set = {< 1, 2, 3 >}
            assert(1 in test_set)
            assert(not (4 in test_set))
        }
        """
        self._run_code(test_code)

    def test_bytes_indexing(self):
        test_code = """
        # Helper test func to check calls with list parameters:
        func test_f(list_ref) {
            return list_ref[0]
        }

        # Main test:
        func main() {
            # Check our intended, different (to python3) indexing semantics:
            assert(b"a"[0] == b"a")

            # Check that slicing still works:
            assert(b"abc"[0:2] == b"ab")

            # Make sure that list parameters still work:
            assert(test_f([1, 2]) == 1)
        }
        """
        self._run_code(test_code)

    def test_string_addition(self):
        test_code = """
        class LocationTest {
            let line = 5
        }
        func main() {
            let e = ""
            let error = {
                "location" : LocationTest()
            }

            # Test combining number and string in complex scenario:
            e += error["location"].line + ":"
            assert(e == "5:")

            # Make sure operator precedence with > works:
            result = 5 > 4 + 3
            assert(result == false,
                "+ is required to bind closer than > operator")
            return 0
        }
        """
        self._run_code(test_code)

if __name__ == "__main__":
    unittest.main()

