#!/usr/bin/env python3

'''
tethercode Baseline Runner
Copyright (c) 2016-2017, Jonas Thiem et al. (see AUTHORS.md)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

SPDX-License-Identifier: Zlib
'''

"""
This file contains the implementation of the tethercode Baseline Runner in
Python 3.

It will do the following:

- Translate the specified tethercode source code to Python 3 source code and
  store the result in a temporary file IN A VERY HACKY AND UNRELIABLE WAY
  THAT ONLY WORKS FOR A SPECIAL SUBSET OF TETHERCODE PROGRAMS

- Run that temporary file instantly after generation to execute the given
  tethercode program

DONT USE THIS TRANSLATOR FOR ARBITRARY TETHERCODE PROGRAMS. USE IT ONLY IF
YOU KNOW WHAT YOU ARE DOING.
This translator is VERY HACKY and lacks most reasonable error or sanity
checks, and will do the following:

- give useless, lacking or no error messages for various invalid code and
  possibly run it partially and fail or misbehave spectacularly

- not run various valid tethercode although a correct implementation
  should

IF THIS TRANSLATOR RUNS YOUR PROGRAM INCORRECTLY OR REFUSES TO RUN IT, THIS
DOESN'T MEAN IN ANY WAY IT ISN'T CORRECT TETHERCODE. IT IS PURELY MEANT TO AID
BOOTSTRAPPING THE BASE TOOLS.

---
Bugs: you can report bugs if they're really simple to address or if they
actually impact the compiler or standard lib. Anything else you are still
free to report, but it will probably not get addressed due to the very
limited scope and hacky nature of this script.

"""

import argparse
import copy
import logging
import os
import platform
import shutil
import subprocess
import sys
import tempfile
import textwrap

PROGRAM_VERSION="3"

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

def is_bin_op(c):
    return (c in set([
        "+", "==", "=", "*", "-", "/",
        "&", "%", "|", "~",
        "and", "or", "not", "in"]))

class ErrorCheckAndPrecompileDefParser(object):
    def __init__(self, s, file_name="<unknown>",
            full_path=None):
        self.file_name = file_name
        self.full_path = full_path
        new_s = ""
        i = 0
        backslash_escaped = False
        in_quotation = None
        in_quotation_since = None
        in_line_comment = False
        block_open_locations = []
        last_block_close = None
        inside_expression = None
        class_nesting_level = None
        line = 1
        column = 0

        # Bracket tracking info:
        bracket_nestings = []
        last_bracket_close_of_type = dict()

        # Go through source file:
        while i < len(s):
            c = s[i]
            new_s += c
            column += 1
            if c == "\n":
                column = 0
                line += 1

            # Handle line comments:
            if in_line_comment:
                if c == "\n":
                    in_line_comment = False
                else:
                    i += 1
                    continue
            elif c == "#" and in_quotation == None:
                in_line_comment = True
                i += 1
                continue

            # Handle string quotations:
            if c == "'" or c == "\"":
                if c == in_quotation:
                    if backslash_escaped:
                        backslash_escaped = False
                        i += 1
                        continue
                    in_quotation = None
                    in_quotation_since = None
                elif in_quotation == None:
                    in_quotation = c
                    in_quotation_since = (line, column)
                    i += 1
                    continue

            # Handle backslash escaping:
            if in_quotation != None and c == "\\":
                if backslash_escaped:
                    backslash_escaped = False
                    i += 1
                    continue
                else:
                    backslash_escaped = True
                    i += 1
                    continue
            elif in_quotation != None and c != "\\":
                backslash_escaped = False

            # Handle code block nesting (for error checking):
            if c == "{" and in_quotation == None:
                inside_expression = None
                block_open_locations.append((line, column))
            elif c == "}" and in_quotation == None:
                inside_expression = None
                if len(block_open_locations) <= 0:
                    t = (self.file_name + ":" + str(line) + ":" +
                        str(column) + ": unexpected '}' block close: ")
                    if last_block_close != None:
                        t += ("most outer block was already closed in " +
                            "line " + str(last_block_close[0]) + ", " +
                            "column " + str(last_block_close[1]))
                    else:
                        t += ("no block was opened yet that could " +
                            "be closed")
                    raise SyntaxError(t)
                last_block_close = (line, column)
                block_open_locations = block_open_locations[:-1]
                if class_nesting_level != None:
                    if (len(block_open_locations) <=
                            class_nesting_level):
                        class_nesting_level = None

            # Handle bracket nesting (for error checking):
            if c == "{" or c == "(" or c == "[":
                if in_quotation == None:
                    bracket_nestings.append((c, line, column))
            elif (c == "}" or c == ")" or c == "]") and in_quotation == None:
                open_type = "{"
                if c == ")":
                    open_type = "("
                elif c == "]":
                    open_type = "["
                t = (self.file_name + ":" + str(line) + ":" +
                    str(column) + ": unexpected '" + c + "':")
                if len(bracket_nestings) == 0:
                    if c in last_bracket_close_of_type:
                        t += (" last bracket expression of " +
                            "type '" + open_type + "' was closed " +
                            "in line " + str(
                            last_bracket_close_of_type[c][0]) + ", " +
                            "column " + str(
                            last_bracket_close_of_type[c][1]))
                        raise SyntaxError(t)
                    else:
                        t += (" there hasn't been any bracket " +
                            "expression of type '" + open_type +
                            "' that could be closed")
                    raise SyntaxError(t)
                elif bracket_nestings[-1][0] != open_type:
                    correct_close = "}"
                    if bracket_nestings[-1][0] == "(":
                        correct_close = ")"
                    elif bracket_nestings[-1][0] == "[":
                        correct_close = "]"
                    t += (" expected '" + correct_close +
                        "' closing '" + bracket_nestings[-1][0] +
                        "' expression starting in line " +
                        str(bracket_nestings[-1][1]) + ", column " + str(
                        bracket_nestings[-1][2]))
                    raise SyntaxError(t)
                last_bracket_close_of_type[c] = (line, column)
                bracket_nestings = bracket_nestings[:-1]

            # See if this is a token start:
            token_separator = [ "(", ")", ",", "+", "/", "-",
                "*", "]", "[", "{", "}", ":", ";", ">", "<",
                "=", "^", "%", "'", "\"", " ", "\t", "\n", "\r",
                "!" ]
            is_token_start = False
            if in_quotation == None and \
                    (len(new_s) < 2 or new_s[-2] in token_separator):
                is_token_start = True
            def has_token(token):
                nonlocal s
                nonlocal token_separator
                nonlocal i
                nonlocal in_quotation
                if (in_quotation == None and
                    (len(new_s) < 2 or new_s[-2] in token_separator)
                        and s[i:].startswith(token)
                        and (len(s[i:]) <= len(token) or
                        s[i+len(token)] in token_separator)):
                    return True
                elif s.startswith(token):
                    print("FALSE POSITIVE [" + str(token) +
                        "]: " + s[:len(token) * 2], flush=True)
                return False
            def extract_token():
                k = i
                while k < len(s):
                    if s[k] in token_separator:
                        break
                    k += 1
                return s[i:k]

            # Handle the precompiler tokens we are supposed to replace:
            if has_token("precconst_currpos"):
                new_s = (new_s[:-1] + "\"" +
                    file_name.replace("\\", "\\\\").replace("\"",
                    "\\\"").replace("\n", "\\n").replace("\r",
                    "\\r").replace("\t", "\\t") + ":" +
                    str(line) + ":" + str(column) + "\""
                    )
                i += len("precconst_currpos")
                continue
            elif has_token("precconst_currfilepath"):
                new_s = (new_s[:-1] + "'" + str(self.file_name).replace(
                    "'", "\\'") + "'")
                i += len("precconst_currfilepath")
                continue
            elif has_token("precconst_baselinepath"):
                new_s = (new_s[:-1] + "'" + str(
                        os.path.abspath(__file__)
                    ).replace(
                    "'", "\\'") + "'")
                i += len("precconst_baselinepath")
                continue

            # Check for "if", "for", "while", "elseif" for proper condition
            # endings:
            if (has_token("for") or has_token("while") or
                    has_token("if") or has_token("elseif")):
                if inside_expression != None:
                    if not has_token("if"):
                        raise SyntaxError(
                            self.file_name + ":" + str(line) + ":" +
                            str(column) + ": unexpected '" +
                            str(extract_token()) +
                            "' inside expression '" + str(inside_expression)
                            + "'")
                else:
                    if has_token("for") or has_token("while"):
                        inside_expression = extract_token()
                        i += 1
                        continue

            # Check invalid expression nestings:
            if (has_token("let") or has_token("while") or
                    has_token("elseif") or has_token("for")):
                if inside_expression != None:
                    raise SyntaxError(
                        self.file_name + ":" + str(line) + ":" +
                        str(column) + ": unexpected '" +
                        str(extract_token()) +
                        "' inside expression '" + str(inside_expression)
                        + "'")

            # Check missing } in front of catch:
            if has_token("catch"):
                if not new_s[:-1].strip().endswith("}"):
                    raise SyntaxError(
                        self.file_name + ":" + str(line) + ":" +
                        str(column) + ": missing '}' in front of " +
                        "'catch' statement")

            # Outdated code helper:
            if has_token("then") or has_token("do") or has_token("function") \
                    or has_token("end"):
                print(self.file_name + ":" + str(line) + ":" +
                    str(column) + ": WARNING: you used " +
                    "an outdated token '" + extract_token() + "' - " +
                    "did you forget to transition old code?",
                    file=sys.stderr, flush=True)

            # Nested class tracking (since baseline runner can't do this):
            if has_token("class"):
                if class_nesting_level != None:
                    raise SyntaxError(
                        self.file_name + ":" + str(line) + ":" +
                        str(column) + ": nested 'class' inside " +
                        "'class' - baseline runner doesn't support " +
                        "this feature")
                class_nesting_level = len(block_open_locations)
            i += 1
        self.s = new_s
        if in_quotation != None:
            raise SyntaxError(self.file_name + ":" + str(line) + ":" +
                str(column) + ": unterminated string literal " +
                "at end of file " +
                "which was opened in line " + str(in_quotation_since[0])
                + ", column: " + str(in_quotation_since[1]))
        if len(block_open_locations) > 0:
            raise SyntaxError(self.file_name + ":" + str(line) + ":" +
                str(column) + ": unclosed block at end of file " +
                "which was opened in line " +
                str(block_open_locations[-1][0]) + ", column " +
                str(block_open_locations[-1][1]))

    def __call__(self):
        return self.s

class PreImportParser(object):
    """ Takes a wobbly source code in a string, and tries to resolve all
        import statements. Returns a dictionary with the keys being relative
        code paths the imported files should have, and the values being the
        full source code in each of the imported files. The imports will also
        be renamed (both in the relative file paths and in the provided source
        code) to avoid clashes with python standard lib modules - however,
        with import X as Y syntax to make them usable under the original name
        again in the actual source beyond the import statements.

        Import statements that cannot be resolved to existing files will be
        left untouched by this parser.
    """

    def __init__(self, s, file_path, base_paths=[], global_modules=[],
            _already_imported=set(), unique_counter=0,
            packages_dir="tc_packages",
            full_file_path=None):
        assert(full_file_path != None)
        self._already_imported = _already_imported
        self.global_modules = global_modules
        if platform.system().lower() == "windows":
            packages_dir = packages_dir.replace("/", os.path.sep)
        self.packages_dir = os.path.normpath(packages_dir)
        assert(os.path.exists(packages_dir))
        self.file_path = file_path
        self.unique_counter = 0
        self.s = ErrorCheckAndPrecompileDefParser(s, file_path,
            full_path=full_file_path)()
        self.full_file_path = full_file_path
        dir_base_paths = []
        for base_path in base_paths:
            if os.path.exists(base_path) and not os.path.isdir(base_path):
                dir_base_paths.append(os.path.dirname(base_path))
            else:
                dir_base_paths.append(base_path)
        self.bases = [os.path.abspath(base_path) for base_path in\
            dir_base_paths]

    def __call__(self):
        result_files = dict()
        result_lines = []
        lines = self.s.replace("\r\n", "\n").replace("\r", "\n").split("\n")
        for line in lines:
            # Translate pythonlang.do_import("a.b.c") to: import a.b.c
            if line.lstrip().startswith("pythonlang.do_import("):
                imported = line.partition("(")[2].partition(")")[0]
                imported_as = imported
                if imported.find(",") > 0:
                    imported_as = imported.partition(",")[2]
                    imported = imported.partition(",")[0]
                # Cut quotes:
                imported = imported.strip()[1:-1]
                imported_as = imported_as.strip()[1:-1]
                result_lines.append("import " + imported + " as " +
                    imported_as)
                continue

            # Throw away import pythonlang statements:
            if line.lstrip() == "import pythonlang":
                continue

            # Process regular imports:
            if line.lstrip().startswith("import "):
                # See if this is an import statement we can process:
                import_name = line.lstrip()[len("import "):].strip()

                if (import_name.find("..") >= 0 or import_name.find("/") >= 0
                        or import_name.find(" as ") >= 0):
                    result_lines.append(line)
                    continue  # invalid/unexpected, ignore it and resume
                if import_name.startswith("."):
                    result_lines.append(line)
                    continue  # invalid/unexpected, ignore it and resume

                # Generate an altered import statement with altered paths to
                # avoid python import module clashes
                def process_path(f):
                    parts = f.split(".")
                    return ".".join(["_tethercode_imported_" + part
                        for part in parts])
                if import_name.find(".") < 0:
                    # Import as altered name, then rename:
                    #full_parts = process_path(import_name).split(".")
                    new_line = ("import " + process_path(
                        import_name) + " as " +
                        import_name)
                else:
                    # Manually construct sub objects of altered import path:
                    count = self.unique_counter
                    new_line = ("import " + process_path(
                        import_name) + " as " +
                        "_altered_full_path_import_" +
                        str(count) + "_" +
                        import_name.rpartition(".")[2])
                    self.unique_counter += 1
                    existing_path = ""
                    for item in import_name.rpartition(".")[0].split("."):
                        if len(existing_path) == 0:
                            new_line += (
                                "\nif not '" + item +
                                    "' in locals() {"
                                "\n    " + item +
                                " = lambda: None" +
                                "\n}")
                            existing_path = item
                        else:
                            new_line += (
                                "\nif not hasattr(" + existing_path +
                                    ", '" + item + "') {"
                                "\n    " + existing_path + "." + item +
                                " = lambda: None" +
                                "\n}")
                            existing_path += "." + item
                    new_line += ("\n" + import_name +
                        " = _altered_full_path_import_" + str(count) +
                        "_" + import_name.rpartition(".")[2])

                # Construct file paths:
                full_path = None
                import_path_source = import_name.replace(".", "/") + ".tc"
                import_path_target = process_path(import_name).replace(
                    ".", "/") + ".tc"
                import_parts = import_name.split(".")
                assert(os.path.exists(self.packages_dir))
                for base in self.bases + [os.path.abspath(
                        os.path.join(self.packages_dir, f)) for f in \
                        os.listdir(
                        self.packages_dir) if os.path.exists(os.path.join(
                            self.packages_dir, f)) and os.path.isdir(
                            os.path.join(self.packages_dir, f)) and \
                            f != "bin" and f != "cache"]:
                    full_path = os.path.join(base, import_path_source)
                    if os.path.exists(full_path):
                        break
                    else:
                        full_path = None

                # Try the standard lib if no local file matches:
                if full_path == None and import_path_source != None:
                    p = os.path.join(
                        self.packages_dir, "io.tethercode.stdlib",
                            import_path_source)
                    if os.path.exists(p):
                        full_path = p

                # Skip this import statement if we can't find the file:
                if full_path == None or not os.path.exists(full_path):
                    result_lines.append(line)
                    continue

                # Make sure not to import twice:
                if (self.file_path, os.path.normpath(full_path)) in \
                        self._already_imported:
                    continue

                # Go ahead and mark this import as done:
                self._already_imported.add((self.file_path, full_path))
                result_lines.append(new_line)
 
                # Open file and get the code import-processed, and add all
                # the results to our result tree:
                with open(full_path, "r") as f:
                    contents = f.read()
                new_stuff = PreImportParser(contents, import_path_source,
                    base_paths=[full_path] + self.bases,
                    global_modules=import_parts,
                    _already_imported=self._already_imported,
                    unique_counter=self.unique_counter,
                    packages_dir=self.packages_dir,
                    full_file_path=full_path)()
                self.unique_counter += 100
                for item_path in new_stuff:
                    if not item_path in result_files:
                        result_files[item_path] = new_stuff[item_path]

                continue
            result_lines.append(line)
        result_files[self.file_path] = ("\n".join(result_lines),
            self.full_file_path)
        return result_files

class PrecompileCheckParser(object):
    """ Finds precompile_check_if statements and evaluates them in a really
        crappy and incorrect way. Don't rely on this working correctly, it is
        barely good enough to work for the standard library and compiler.
    """
    def __init__(self, s, file_name="<unknown>", base_dir=None,
            verbose=False):
        self.s = s
        self.file_name = file_name
        self.base_dir = base_dir
        self.verbose = verbose

    def evaluate_condition(self, condition):
        condition = condition.strip()

        # Replace known variables:
        while condition.find("translator_name") >= 0:
            condition = condition.replace("translator_name",
                "\"baseline\"")
        while condition.find("DEBUG") >= 0:
            condition = condition.replace("DEBUG", "true")

        # Replace path_exists() checks:
        replaced_something = True
        while replaced_something:
            replaced_something = False
            i = 0
            while i < len(condition):
                if condition[i:].startswith("path_exists("):
                    condition_start = i
                    path_start = i + len("path_exists(")
                    i += len("path_exists(")
                    while i < len(condition) and condition[i] != ")":
                        i += 1
                    path_extract = condition[path_start+1:i - 1]
                    check_path = os.path.join(os.path.dirname(
                        self.file_name),
                        path_extract)
                    if self.base_dir != None and \
                            not os.path.isabs(check_path):
                        check_path = os.path.join(self.base_dir,
                            path_extract)
                    if self.verbose:
                        print("tools/baseline-runner.py: verbose: " +
                            "Processing path_exists() precompiler check " +
                            "on: " + str(check_path) + " with result: " +
                            str(os.path.exists(check_path)))
                    if os.path.exists(check_path):
                        result = "true"
                    else:
                        result = "false"
                    condition = condition[:condition_start] + result +\
                        condition[i + 1:]
                    replaced_something = True
                    break
                i += 1

        def _remove_unneeded_whitespace_forward(condition):
            def reqs_space(c):
                c = c[:1]
                if c == "=" or c == ")" or \
                        c == "(" or c == "\"" or \
                        c == "-":
                    return False
                return True
            new_condition = ""
            previous_needs_space = False
            previous_was_space = False
            quoted = None
            i = 0
            while i < len(condition):
                if quoted != None:
                    if condition[i] == quoted:
                        quoted = None
                    new_condition += condition[i]
                    i += 1
                    continue
                if condition[i] == "\"" or condition[i] == "'":
                    quoted = condition[i]
                if condition[i] == " ":
                    if previous_needs_space:
                        new_condition += condition[i]
                        previous_needs_space = False
                    i += 1
                    continue
                previous_needs_space = False
                if reqs_space(condition[i]):
                    previous_needs_space = True
                new_condition += condition[i]
                i += 1
            return new_condition.replace(" (", "(").replace(" \"", "\"")

        def remove_unneeded_whitespace(condition):
            condition = _remove_unneeded_whitespace_forward(condition)
            condition = _remove_unneeded_whitespace_forward(condition[::-1])
            return condition[::-1]

        replaced_something = True
        while replaced_something:
            replaced_something = False
            condition = remove_unneeded_whitespace(condition)

            # Replace equal expressions with true:
            i = 0
            while i < len(condition):
                part = condition[i:]
                eq_pos = part.find("=")
                if eq_pos > 0:
                    first_part = part[:eq_pos]
                    if part.startswith(first_part + "==" + first_part):
                        condition = condition[:i] + "true" + condition[
                            i+len(first_part)*2+2:]
                        replaced_something = True
                        break
                i += 1
                continue

        # Replace "not" stuff:
        replaced_something = True
        while replaced_something:
            replaced_something = False
            condition = condition.replace("not true", "false")
            condition = condition.replace("not false", "true")

        # Replace "and" stuff:
        replaced_something = True
        while replaced_something:
            replaced_something = False
            condition = condition.replace("true and true", "true")
            condition = condition.replace("false and true", "false")
            condition = condition.replace("true and false", "false")
            condition = condition.replace("false and false", "false")

        # Replace "or" stuff:
        replaced_something = True
        while replaced_something:
            replaced_something = False
            condition = condition.replace("true or true", "true")
            condition = condition.replace("false or true", "true")
            condition = condition.replace("true or false", "true")
            condition = condition.replace("false or false", "false")

        return remove_unneeded_whitespace(condition)            

    def __call__(self):
        result_lines = []
        current_skip_on_depth = -1
        block_depth = 0
        lines = self.s.split("\n")
        for line in lines:
            _l = line.strip()
            # Just continue if not a preprocessor line:
            if not _l.startswith("preccheck_if") and \
                    not _l.startswith("preccheck_else") and \
                    not _l.startswith("preccheck_end"):
                if current_skip_on_depth < 0:
                    result_lines.append(line)
                continue
            line = line.strip()

            # Parse preprocessor line:
            if line.startswith("preccheck_if"):
                block_depth += 1
                if current_skip_on_depth >= 0:
                    # Ignore, we're already skipping anyway
                    continue
                result = self.evaluate_condition(
                    line[len("preccheck_if"):].strip())
                if result != "true":
                    assert(result == "false"), \
                        "unexpected precompiler result: " + str(result) +\
                        " - with evaluated condition: " + str(
                        line[len("preccheck_if"):].strip())
                    current_skip_on_depth = block_depth
            elif line.startswith("preccheck_else"):
                if current_skip_on_depth >= block_depth:
                    current_skip_on_depth = -1
                else:
                    current_skip_on_depth = block_depth
            elif line.startswith("preccheck_end"):
                if current_skip_on_depth >= block_depth:
                    current_skip_on_depth = -1
                block_depth -= 1
                assert(block_depth >= 0),\
                    ("preccheck_* blocks must be closed properly "+
                    "with correct nesting in file " + self.file_name)
        result = "\n".join(result_lines)
        #print("COMPLETE RESULT:")
        #print(result)
        #print("END OF COMPLETE RESULT")
        #print("", flush=True)
        return result

class Tokenizer(object):
    """ Splits up the source code into separate meaningful tokens. It will
        remove all non-significant whitespace except for line breaks.

        The tokens are simply generated as a list of strings, with each
        string being a separate token.
    """

    def __init__(self, s):
        self.s = s
        self.include_linebreaks = True
        self.round_bracket_depth = 0
        self.just_yielded_linebreak = False

    def __next__(self):
        t = self._next_token_internal()
        if t == "\n" and not self.include_linebreaks:
            return self.__next__()
        if t == "\n":
            if self.just_yielded_linebreak:
                return self.__next__()
            self.just_yielded_linebreak = True
        else:
            self.just_yielded_linebreak = False
        return t

    def _next_token_internal(self):
        if len(self.s) == 0:
            raise StopIteration()
        if self.s.startswith(" ") or self.s.startswith("\t") or \
                self.s.startswith("\r") or (self.round_bracket_depth > 0 and\
                self.s.startswith("\n")):
            self.s = self.s[1:]
            return self._next_token_internal()
        if self.s.startswith("\n"):
            self.s = self.s[1:]
            return "\n"
        if self.s.startswith("#"):
            while not self.s.startswith("\n") and not self.s.startswith(
                    "\r") and len(self.s) >= 1:
                self.s = self.s[1:]
            return self._next_token_internal()

        # Treat a {} dictionary initializer as a single thing:
        if self.s.startswith("{}"):
            self.s = self.s[2:]
            return "{}"

        # Treat a [] list initializer as a single thing:
        if self.s.startswith("[]"):
            self.s = self.s[2:]
            return "[]"

        # Treat != expression as one element:
        if self.s.startswith("!="):
            self.s = self.s[2:]
            return "!="

        # Treat let:const as let
        if self.s.startswith("let:const"):
            self.s = self.s[len("let:const"):]
            return "let"

        # Ignore :unmanaged
        if self.s.startswith(":unmanaged"):
            self.s = self.s[len(":unmanaged")]
            return self._next_token_internal()

        # String literals:
        if self.s.startswith("\"") or self.s.startswith("'"):
            i = 1
            end_char = self.s[0]
            escaped = False
            while True:
                if not escaped and self.s[i] == end_char:
                    result = self.s[:i+1]
                    self.s = self.s[i+1:]
                    return result
                if self.s[i] == "\\" and not escaped:
                    escaped = True
                else:
                    escaped = False
                i += 1
            raise RuntimeError("unexpected missing end of string literal")

        # Binary string literals:
        if self.s.startswith("b\"") or self.s.startswith("b'"):
            i = 2
            end_char = self.s[1]
            escaped = False
            while True:
                if not escaped and self.s[i] == end_char:
                    result = self.s[:i+1]
                    self.s = self.s[i+1:]
                    return result
                if self.s[i] == "\\" and not escaped:
                    escaped = True
                else:
                    escaped = False
                i += 1
            raise RuntimeError("unexpected missing end of bytestring literal")

        # Check if something starts with a digit:
        def is_digit(s):
            if len(s) < 1:
                return False
            if ord(s[0]) >= ord('0') and ord(s[0]) <= ord('9'):
                return True
            return False


        # Numeric literals:
        def is_positive_number(value):
            if is_digit(value):
                alphanumeric = False
                i = 1
                if value.startswith("0x"):
                    alphanumeric = True
                    i += 2
                while is_digit(value[i:]) or (alphanumeric and
                        ((ord(value[i]) >= ord("a") and
                        ord(value[i]) <= ord("f")) or
                        (ord(value[i]) >= ord("A") and
                        ord(value[i]) <= ord("F")))) :
                    i += 1
                if len(value[i:]) > 1 and value[i] == "." and \
                        is_digit(value[i+1]) and not alphanumeric:
                    i += 1
                    while is_digit(value[i:]):
                        i += 1
                return i
            return -1
        _len = is_positive_number(self.s)
        if _len > 0:
            result = self.s[:_len]
            self.s = self.s[_len:]
            return result
        if self.s.startswith("-"):  # negative numbers
            _len = is_positive_number(self.s[1:])
            if _len > 0:
                result = self.s[:_len+1]
                self.s = self.s[_len+1:]
                return result

        # Keywords:
        for k in [
            "if", "func", "end", "let", "import",
            "class", "throw", "try", "catch", "as",
        ]:
            stopper = [ " ", "\t", "," "(", ")", "\n", "\r" ]
            for stop in stopper:
                if self.s.startswith(k + stop):
                    self.s = self.s[len(k):]
                    return k
            if self.s == k:
                self.s = ""
                return k

        # Custom names:
        def valid_custom_name_char(c, is_first):
            if len(c) == 0:
                return False
            if not is_first and is_digit(c[0]):
                return True
            if c[0] == "_":
                return True
            if ord(c[0]) >= ord("a") and ord(c[0]) <= ord("z"):
                return True
            if ord(c[0]) >= ord("A") and ord(c[0]) <= ord("Z"):
                return True
            return False
        if valid_custom_name_char(self.s, True):
            i = 1
            while i < len(self.s) and valid_custom_name_char(
                    self.s[i:], False):
                i += 1
            result = self.s[:i]
            self.s = self.s[i:]
            return result

        # Bracket and various operator expressions:
        misc = [ "{<", ">}",
            "(", ")", "=", "+", "-", "/", "*", ".",
            "]", "[", ",", "<", ">", ":", "}", "{",
            "%", "&", "|" ]
        for m in misc:
            if self.s.startswith(m):
                if m == "(":
                    self.round_bracket_depth += 1
                elif m == ")":
                    self.round_bracket_depth -= 1
                result = self.s[:len(m)]
                self.s = self.s[len(m):]
                return result

        raise RuntimeError("unexpected token starting with: " +\
            str(self.s[:30]))

    def __iter__(self):
        return self

    def __call__(self):
        return list(self)


class LinebreakCleanupTokenizer(object):
    """
        This processor drops all inline line breaks that are inside bracketed
        expressions in a given token stream (e.g. as generated by the
        Tokenizer).

        Keep in mind the line breaks are actually meant to be not significant
        in wobbly, but they are kept here because the baseline runner relies
        on them to be significant (which works for the standard library and
        compiler since they respect this limitation of the baseline runner).
    """

    def __init__(self, tokenizer):
        self.tokenizer = tokenizer
        self._round_bracket_depth = 0
        self._square_bracket_depth = 0
        self._curly_bracket_depth = 0
        self.tokens = [token for token in self.tokenizer]
        self.i = 0

    def __next__(self):
        # StopIteration when reaching the end:
        i = self.i
        if i >= len(self.tokens):
            raise StopIteration ()

        # Process token:
        t = self.tokens[i]
        t_next = ""
        if i + 1 < len(self.tokens):
            t_next = self.tokens[i + 1]
        t_prev = ""
        if i > 0:
            t_prev = self.tokens[i - 1]
        tokens = self.tokens
        self.i += 1
        if t == "(":
            self._round_bracket_depth += 1
        elif t == ")":
            self._round_bracket_depth -= 1
        elif t == "[":
            self._square_bracket_depth += 1
        elif t == "]":
            self._square_bracket_depth -= 1
        elif t == "{":
            self._curly_bracket_depth += 1
        elif t == "}":
            self._curly_bracket_depth -= 1
        elif t.startswith("'") or t.startswith("\""):
            t = t.replace("\r\n", "\n")
            while t.find("\\\n") >= 0:
                t = t.replace("\\\n", "\\\\\n")
            t = t.replace("\n", "\\n")
        
        # Skip newlines which are lead or trailed by binary operators:
        if t == "\n" and (is_bin_op(t_next) or
                t_next == "." or t_next == "[" or
                is_bin_op(t_prev) or
                t_prev == "." or t_prev == "["):
            return self.__next__()

        # Skip newlines with other obvious hints of the next being inline:
        if t == "\n" and (
                t_next == "(" or t_next == "\"" or t_next == "'"):
            return self.__next__()

        # Skip line breaks contained inside inline bracket expressions:
        if t == "\n" and (
                self._round_bracket_depth > 0 or
                self._square_bracket_depth > 0 or
                self._curly_bracket_depth > 0):
            # Check if { .. } are for a dictionary constructor,
            # because if not this is a code block and we CAN'T skip:
            no_skip = False
            if self._round_bracket_depth <= 0 and \
                    self._square_bracket_depth <= 0:
                found_colon = False

                # Scan forwards for colon indicating dict constructor:
                cur_brack_depth = 0
                k = i
                while k < len(tokens):
                    if (tokens[k] == "[" or tokens[k] == "(" or
                            tokens[k] == "{"):
                        cur_brack_depth += 1
                    elif (tokens[k] == "]" or tokens[k] == ")" or
                            tokens[k] == "}"):
                        cur_brack_depth -= 1
                        if cur_brack_depth < 0:
                            break
                    if tokens[i] == ":":
                        found_colon = True
                        break
                    k += 1
                # Scan backwards for colon indicating dict constructor:
                cur_brack_depth = 0
                k = i
                while k >= 0:
                    if (tokens[k] == "[" or tokens[k] == "(" or
                            tokens[k] == "{"):
                        cur_brack_depth -= 1
                        if cur_brack_depth < 0:
                            break
                    elif (tokens[k] == "]" or tokens[k] == ")" or
                            tokens[k] == "}"):
                        cur_brack_depth == 1
                    if tokens[i] == ":":
                        found_colon = True
                        break
                    k -= 1
                # If we found no colon, we cannot safely skip:
                if not found_colon:
                    no_skip = True
                else:
                    no_skip = False

            # Skip! (unless we found a reason not to)
            if not no_skip:
                return self.__next__()
        return t

    def __iter__(self):
        return self

    def __call__(self):
        return list(self)

class ListDictTransformTokenizer(object):
    def __init__(self, tokenizer):
        self.tokenizer = tokenizer
        self.include_linebreaks = True
        self.history = []
        self.bracket_depth = 0
        self.close_at_bracket_depth = dict()
        self.next_list = []

    def __next__(self):
        if len(self.next_list) == 0:
            self.next_list = self._next_as_list()
        item = self.next_list[0]
        self.next_list = self.next_list[1:]
        return item

    def _next_as_list(self):
        t = self._next_with_linebreaks()
        if not self.include_linebreaks:
            t = [t for item in t if item != "\n"]
        return t

    def _next_with_linebreaks(self):
        t = self._next_token_internal()
        if type(t) == list:
            self.history += t
            return t
        # See if this is a dict or list constructor:
        if (t == "[" or t == "{") and \
                len(self.history) > 0 and \
                (is_bin_op(self.history[-1]) or
                self.history[-1] == "(" or
                self.history[-1] == "[" or
                self.history[-1] == "{" or
                self.history[-1] == "return"):
            is_dict_or_list = True
            if self.history[-1] == "{":
                is_dict_or_list = False
                i = -2
                while -i <= self.history.length:
                    if self.history[i] == "{":
                        i -= 1
                        continue
                    if is_bin_op(self.history[i]) or \
                            self.history[i] == "return":
                        is_dict_or_list = True
                    break
            if is_dict_or_list:
                # It is a dict or list constructor.
                # Transform to our special types:
                self.close_at_bracket_depth[
                    self.bracket_depth - 1] = ")"
                self.history.append(t)
                if t == "[":
                    return ["_ChiCraftList", "(", t]
                elif t == "{":
                    return ["_ChiCraftDict", "(", t]

        # Handle additional close brackets from our injected
        # special constructor code:
        if t == "]" or t == ")" or t == "}":
            self.history.append(t)
            if self.bracket_depth in \
                    self.close_at_bracket_depth:
                c = self.close_at_bracket_depth[
                    self.bracket_depth]
                del self.close_at_bracket_depth[
                    self.bracket_depth]
                return [t, c]
            return [t]

        self.history.append(t)
        return [t]

    def _next_token_internal(self):
        t = self.tokenizer.__next__()
        if t == "(" or t == "[" or t == "{" or t == "{<":
            self.bracket_depth += 1
        elif t == ")" or t =="]" or t == "}" or t == ">}":
            self.bracket_depth -= 1
        if t == "[]":
            return ["_ChiCraftList", "(", "[]", ")"]
        if t == "{}":
            return ["_ChiCraftDict", "(", "{}", ")"]
        return t

    def __iter__(self):
        return self

    def __call__(self):
        return list(self)

def is_balanced(tokens):
    bracket_depth = 0
    i = 0
    while i < len(tokens):
        t = tokens[i]
        if t == "(" or t == "[" or t == "{" \
                or t == "{<":
            bracket_depth += 1
        elif t == ")" or t == "]" or t == "}" \
                or t == ">}":
            bracket_depth -= 1
            if bracket_depth < 0:
                return False
        i += 1
    return (bracket_depth == 0)

class AssignmentOperatorReplacementTokenizer(object):
    def __init__(self, tokenizer):
        self.tokenizer = tokenizer
        self.tokens = [t for t in self.tokenizer]
        self.inline_bracket_depth = 0
        self.next_list = []
        self.add_before_next_line_break = None
        self.history = []
        self.index = -1

        # For debugging:
        self._currently_wrapping_value = False
        self._wrapped_tokens = []

    def __next__(self):
        if len(self.next_list) == 0:
            self.next_list = self._next_as_list()
        item = self.next_list[0]
        self.next_list = self.next_list[1:]
        return item

    def _next_as_list(self):
        self.index += 1
        if self.index >= len(self.tokens):
            raise StopIteration()

        t = self.tokens[self.index]
        if t == "(" or t == "[" or t == "{<":
            self.inline_bracket_depth += 1
        elif t == ")" or t =="]" or t == ">}":
            self.inline_bracket_depth -= 1
        elif t == "{" and len(self.history) > 0 and \
                (is_bin_op(self.history[-1]) or
                self.history[-1] == "\n" or
                self.history[-1] == "return" or
                self.history[-1] == "(" or
                self.inline_bracket_depth > 0):
            self.inline_bracket_depth += 1
        elif t == "}" and self.inline_bracket_depth > 0:
            self.inline_bracket_depth -= 1

        if t == "=" and self.inline_bracket_depth == 0 and \
                self.index + 1 < len(self.tokens) and \
                self.tokens[self.index + 1] != "=" and \
                len(self.history) > 0 and \
                self.history[-1] != "=" and \
                self.history[-1] != "!" and \
                self.history[-1] != ">" and \
                self.history[-1] != "<":
            self.history.append(t)
            self._wrapped_tokens = []
            self._currently_wrapping_value = True
            self.add_before_next_line_break = ")"
            return [t, "_ChiCraftAssignedValue", "("]
        if t == "return" and self.index + 1 < len(self.tokens) and \
                self.tokens[self.index + 1] != "\n":
            self.history.append(t)
            self._wrapped_tokens = []
            self.inline_bracket_depth = 0   # HACK !! for: if X \n {
            self.add_before_next_line_break = ")"
            self._currently_wrapping_value = True
            return [t, "_ChiCraftReturnedValue", "("]
        if self.add_before_next_line_break != None and \
                t == "\n" and self.inline_bracket_depth == 0:
            v = self.add_before_next_line_break
            self.history.append(v)
            self.history.append(t)
            self.add_before_next_line_break = None
            self._currently_wrapping_value = False
            #print("WRAPPED: " + str(self._wrapped_tokens))
            def desc_tokens(tokens):
                if len(tokens) < 5:
                    return str(tokens)
                return str(tokens[:5]) + " ... "
            assert is_balanced(self._wrapped_tokens),\
                "assigned token contents not balanced: " +\
                desc_tokens(self._wrapped_tokens)
            return [v, "\n", "# HISTORY: " + str(self.history[-5:]) +
                ", FUTURE: " + str(self.tokens[self.index:self.index+5]) +
                "", "\n", t]
        self.history.append(t)
        if self._currently_wrapping_value:
            self._wrapped_tokens.append(t)
        return [t]

    def __iter__(self):
        return self

    def __call__(self):
        return list(self)


class ClassNameExtractor(object):
    """ Takes a token stream, and then collects the names of all classes
        in that token stream. It will respect 'module' blocks and return all
        class names in all nested module namespace variants.

        This effectively builds a global symbol table but just for classes,
        and just for the given token stream.
    """
    def __init__(self, tokens, file_name="<unknown>"):
        self.tokens = list(tokens)
        self.known_class_names = set()
        self.known_global_vars = set()
        self.file_name = file_name
        block_depth = 0
        modules = []
        i = 0
        while i < len(self.tokens):
            token = self.tokens[i]
            # Handle block depth and modules:
            if token == "{":
                block_depth += 1
            elif (token == "}"):
                block_depth -= 1
                assert(block_depth >= 0),\
                    "code blocks must be properly nested and closed in " +\
                    "file: " + str(self.file_name)
                if len(modules) > 0:
                    if modules[-1]["depth"] > block_depth:
                        modules = modules[:-1]
            # Collect classes:
            if token == "class":
                class_name = self.tokens[i + 1]
                self.known_class_names.add(class_name)
                _curr_module = ""
                k = 0
                while k < len(modules):
                    if k > 0:
                        _curr_module += "."
                    _curr_module += modules[k]["name"]
                    self.known_class_names.add(
                        _curr_module + "." + class_name)
                    k += 1
            elif (token == "let" and block_depth == 0 and
                    i < len(self.tokens) - 1):
                self.known_global_vars.add(self.tokens[i + 1])
            i += 1

    def __call__(self):
        return self.known_class_names

class CodeGenerator(object):
    def __init__(self, tokenizer, global_state=None, pretty=False,
            is_inline=False, indent=0):
        self.pretty = pretty
        self._indent = indent
        self.inline = is_inline
        self.tokenizer = tokenizer
        self.global_state = global_state or {}
        self.history = []
        if not "inside_class" in self.global_state:
            self.global_state["inside_class"] = False
        if not "block_level" in self.global_state:
            self.global_state["block_level"] = 0
            self.global_state["most_inner_module_block"] = 0
        if not "module_chain" in self.global_state:
            self.global_state["module_chain"] = []
            self.global_state["module_chain_levels"] = []
        if not "known_class_names" in self.global_state:
            cnames = ClassNameExtractor(self.tokenizer)
            self.global_state["known_global_vars"] = cnames.known_global_vars
            cnames = cnames()
 
            # Add built-in errors:
            cnames.add("ValueException")
            cnames.add("RuntimeException")
            cnames.add("PermissionException")
            cnames.add("NotImplementedException")
            cnames.add("TypeException")
            cnames.add("IOException")

            self.global_state["known_class_names"] = cnames
        if not "known_included" in self.global_state:
            self.global_state["known_included"] = set()

        if global_state == None:
            self.prepend_code = textwrap.dedent('''\
                class MessageExceptionMixin(object):
                    @property
                    def message(self):
                        return self.args[0]

                class ValueException(ValueError, MessageExceptionMixin):
                    def __eq__(self, other):
                        if not hasattr(other, "__class__"):
                            return False
                        if str(other.__class__.__name__) == "ValueException":
                            return True
                        return False

                    def __instancecheck__(self, other):
                        if isinstance(other, ValueError):
                            return True
                        return False

                    def __hash__(self):
                        return 0

                BaseType = lambda: None
                class _ChiCraftNumberTypeComparer(object):
                    def __eq__(self, other):
                        if not isinstance(other, type(dict)):
                            return False
                        if other == int:
                            return True
                        if other == float:
                            return True
                        return False
                BaseType.set = set
                BaseType.number = _ChiCraftNumberTypeComparer()
                BaseType.null = type(None)
                BaseType.boolean = type(True)
                ''') + '_TETHERCODE_TRANSLATOR_NAME_CONSTANT=' +\
                    '"tethercode Baseline Runner, Version ' +\
                    PROGRAM_VERSION + '"\n' +\
                textwrap.dedent('''\
                BaseType.string = type("")
                BaseType.bytes = type(b"")

                try:
                    unused = __orig_print__
                    del(unused)
                except NameError:
                    __orig_print__ = print
                def print(*args, **kwargs):
                    stderr = False
                    if "stderr" in kwargs:
                        stderr = kwargs["stderr"]
                        del(kwargs["stderr"])
                    if "file" in kwargs:
                        raise ValueError(
                            "do you have old code which " +
                            "uses file=sys.stderr?? This " +
                            "was removed in newer baseline " +
                            "runner versions")
                    def transform(arg):
                        if type(arg) == Exception:
                            import traceback
                            arg = str(traceback.format_tb(arg))
                        return arg
                            
                    args = [transform(arg) for arg in args]
                    if stderr:
                        import sys as __print_temp_sys
                        __orig_print__(*args,
                            file=__print_temp_sys.stderr, **kwargs)
                    else:
                        __orig_print__(*args, **kwargs)

                def isinstanceof(v1, v2):
                    return isinstance(v1, v2)

                def char(v):
                    return chr(v)

                def tonumber(v):
                    if type(v) == type(5) or \
                            type(v) == type(5.0):
                        return v
                    if v.find(".") >= 0:
                        return float(v)
                    return int(v)

                class _ChiCraftDict(dict):
                    def __init__(self, orig=None):
                        self.orig = orig or {}

                    def keys(self):
                        return self.orig.keys()

                    def remove(self, k):
                        self.orig.pop(k)

                    def __repr__(self):
                        return self.orig.__repr__()

                    def __str__(self):
                        return str(self.orig)

                    def update(self, *args, **kwargs):
                        self.orig.update(*args, **kwargs)

                    def __getitem__(self, k):
                        return self.orig.__getitem__(k)

                    def __setitem__(self, k, v):
                        self.orig.__setitem__(k, v)

                    def __len__(self):
                        return len(self.orig)

                    def __iter__(self):
                        return self.orig.__iter__()

                    def __contains__(self, k):
                        return self.orig.__contains__(k)

                    def copy(self):
                        return self.orig.copy()

                    def __eq__(self, other):
                        if other is None:
                            return False
                        if not isinstance(other, type({})):
                            return False
                        if len(self) != len(other):
                            return False
                        for k in self.orig:
                            if not k in other or \
                                    self.orig[k] != other[k]:
                                return False
                        return True

                assert(len(_ChiCraftDict({"a":1})) == 1)
                assert(_ChiCraftDict({"a":1}) == {"a" : 1})
                assert("a" in _ChiCraftDict({"a":1}))
                _test_dict = _ChiCraftDict({"a":1})
                _test_dict["a"] = 2
                _test_dict["b"] = 3
                assert("a" in _test_dict)
                assert("b" in _test_dict)

                class _ChiCraftList(list):
                    def add(self, v):
                        super().append(v)

                    def __getitem__(self, v):
                        result = super().__getitem__(v)
                        if result != None and isinstance(result, type([])):
                            return _ChiCraftList(result)
                        return result

                def _ChiCraftAssignedValue(v):
                    if v is None:
                        return v
                    if type(v) == type([]):
                        return _ChiCraftList(v)
                    elif type(v) == type({}):
                        return _ChiCraftDict(v)
                    return v

                assert(_ChiCraftList(_ChiCraftList(
                    [1, 2, 3])) == [1, 2, 3])
                assert([i for i in _ChiCraftList([5, 6])] ==
                    _ChiCraftList([5, 6]))
                assert(_ChiCraftAssignedValue([1, 2, 3])[:-1] == [1, 2])
                assert(_ChiCraftList([1, 2]) + [2, 3] == [1, 2, 2, 3])
                assert(_ChiCraftAssignedValue([1]) == [1])
                assert(len(_ChiCraftList(_ChiCraftList(
                    [1, 2, 3]))) == 3)

                def _ChiCraftReturnedValue(v):
                    return v
                    #return _ChiCraftAssignedValue(v)

                assert(_ChiCraftReturnedValue(None) == None)
                assert(not _ChiCraftReturnedValue(None))

                class _ChiCraftDictTypeComparer(object):
                    def __eq__(self, other):
                        if not isinstance(other, type(dict)):
                            return False
                        if other.__name__.endswith("ChiCraftDict"):
                            return True
                        if other == dict:
                            return True
                        return False
                class _ChiCraftListTypeComparer(object):
                    def __eq__(self, other):
                        if not isinstance(other, type(dict)):
                            return False
                        if other == list:
                            return True
                        if other.__name__.endswith("ChiCraftList"):
                            return True
                        return False
                BaseType.list = _ChiCraftListTypeComparer()
                assert(list == BaseType.list)
                assert(type(_ChiCraftList()) == BaseType.list)
                assert(dict != BaseType.list)
                assert(type("") != BaseType.list)
                assert(type(_ChiCraftDict()) != BaseType.list)
                BaseType.dictionary = _ChiCraftDictTypeComparer()
                def byte(c):
                    return bytes([c])
                _orig_type_func = type
                def type(var):
                    if _orig_type_func(var) == _orig_type_func(int(0)):
                        return _orig_type_func(1.0)
                    return _orig_type_func(var)
                _pythonlang_builtin_open = open
                class _ChiCraftBaseObj(object):
                    def __hash__(self):
                        return 0

                    def __eq__(self, other):
                        if hasattr(self, "equals"):
                            return self.equals(other)
                        return super().__eq__(other)

                    def __neq__(self, other):
                        return not self.__eq__(other)

                def _wobbly_key_dict_handler(k, v):
                    if (isinstance(k, bytes) or isinstance(k, bytearray)) \
                            and isinstance(v, int):
                        return k[v:v + 1]
                    return k[v]
                def _wobbly_addition_handler(*args):
                    have_str = False
                    for arg in args:
                        if arg == None:
                            raise TypeError("cannot have " +
                                "None value as operand in addition")
                        if type(arg) == str:
                            have_str = True
                            break
                    if have_str:
                        new_args = []
                        for arg in args:
                            if (type(arg) == int or
                                    type(arg) == float or
                                    type(arg) == bool):
                                new_args.append(str(arg).lower())
                                continue
                            new_args.append(arg)
                        args = new_args
                    result = None
                    for arg in args:
                        if result == None:
                            result = arg
                        else:
                            result += arg
                    return result
                def _wobbly_tostring(obj):
                    if (hasattr(obj, "tostring") and
                            hasattr(obj.tostring, "__self__")):
                        return obj.tostring()
                    return str(obj)
                ''')
        else:
            self.prepend_code = ""

    @property
    def indent(self):
        return " " * (self._indent * 4)

    @staticmethod
    def is_balanced(tokens):
        c = 0
        for token in tokens:
            if token == "(" or token == "[" or token == "{":
                c += 1
            elif token == ")" or token == "]" or token == "}":
                c -= 1
        return (c == 0)

    @staticmethod
    def get_end_of_same_depth(tokens, include_end=False):
        depth = 0
        i = 0
        while i < len(tokens):
            if tokens[i] == "}":
                depth -= 1
                if depth < 0:
                    if include_end:
                        return (i + 1)
                    return i
            elif tokens[i] == "{":
                depth += 1
            i += 1
        return i

    @staticmethod
    def get_end_of_bracket(tokens,
            end_at_negative_index=False):
        bracket_depth = 0
        i = 0
        while i < len(tokens):
            if (tokens[i] == "[" or tokens[i] == "(" or tokens[i] == "{"
                    or tokens[i] == "{<"):
                bracket_depth += 1
            elif (tokens[i] == "]" or tokens[i] == ")" or tokens[i] == "}"
                    or tokens[i] == ">}"):
                bracket_depth -= 1
                if ((bracket_depth <= 0 and not end_at_negative_index) or
                        bracket_depth < 0):
                    if bracket_depth < 0:
                        return i
                    return i + 1
            elif tokens[i] == "\n" and bracket_depth == 0:
                return i
            elif tokens[i] == "," and bracket_depth == 0:
                return i
            elif i > 0 and (
                    tokens[i] in set({
                        "end", "func", "if", "while", "for",
                        "let"
                    })):
                return i
            i += 1
        return i

    @staticmethod
    def get_end_of_expr(tokens):
        if len(tokens) > 0 and tokens[0] in set([
                "+", "-", "*", "/", "^", "&",
                "|", "or", "and"
                ]) or (tokens[0].startswith("!") or
                tokens[0].startswith("=") or
                tokens[0].startswith(">") or
                tokens[0].startswith("<")):
            return 1
        bracket_depth = 0
        i = 0
        while i < len(tokens):
            if (tokens[i] == "[" or tokens[i] == "(" or tokens[i] == "{"
                    or tokens[i] == "{<"):
                bracket_depth += 1
            elif (tokens[i] == "]" or tokens[i] == ")" or tokens[i] == "}"
                    or tokens[i] == ">}"):
                bracket_depth -= 1
                if (bracket_depth < 0):
                    return i
            elif i > 0 and (tokens[i] == "," or tokens[i] == "\n" or
                    tokens[i] == "+" or tokens[i] == "-" or
                    tokens[i] == "*" or tokens[i] == "/" or
                    tokens[i] == "&" or tokens[i] == "^" or
                    tokens[i] == "~" or tokens[i] == "|" or
                    tokens[i] == ":" or tokens[i] == "or" or
                    tokens[i] == "and"
                    or tokens[i].startswith("=")
                    or tokens[i].startswith("!")
                    or tokens[i].startswith("<")
                    or tokens[i].startswith(">")
                    ) and bracket_depth == 0:
                return i
            i += 1
        return i

    @staticmethod
    def _translate_tokens_for_dict_indexing(tokens):
        # Bail out if not possibly ...[X] expression:
        if len(tokens) < 1 or not tokens[-1] == "]":
            return None

        # Boil out of obvious non-fitting expression:
        if tokens[0] in ("func", "class", "extends",
                "+", "-", "*", "/", "%", "^", "&", "|", "~",
                "in", "->", "and", "or", "not",
                "while", "if", "for", "let",
                "try", "catch", "multiarg", ":", ",",
                "=", "!") or (tokens[0].startswith(">") or
                tokens[0].startswith("<") or
                tokens[0].startswith("!") or
                tokens[0].startswith("=")):
            return None

        # Find last [ char at zero bracket depth:
        last_index = -1
        bracket_depth = 0
        i = len(tokens) - 1
        while i >= 0:
            if bracket_depth <= 1 and tokens[i] == "[":
                last_index = i
                break
            if tokens[i] == "[" or tokens[i] == "(" or tokens[i] == "{":
                bracket_depth -= 1
            elif tokens[i] == "]" or tokens[i] == ")" or tokens[i] == "}":
                bracket_depth += 1
            i -= 1
        if last_index <= 0:
            return None

        # Bail out if [ looks like it's a list:
        prevt = tokens[last_index - 1]
        if prevt == "(" or prevt == "+" or prevt == "-" or \
                prevt == "/" or prevt == "*" or prevt == "," or \
                prevt == "or" or prevt == "and" or prevt == "=" or \
                prevt == "not":
            return None

        # Make sure end part has no : slicing:
        k = i + 1
        bracket_depth = 0
        while k < len(tokens) - 1:
            if tokens [k] == "(" or tokens [k] == "[" or \
                    tokens [k] == "{":
                bracket_depth += 1
            elif tokens [k] == ")" or tokens [k] == "]" or \
                    tokens [k] == "]":
                bracket_depth -= 1
            if bracket_depth == 0 and tokens [k] == ":":
                return None
            k += 1

        # Make sure start part has no forbidden binary ops:
        k = 0
        bracket_depth = 0
        while k < last_index:
            if tokens [k] == "(" or tokens [k] == "[" or \
                    tokens [k] == "{":
                bracket_depth += 1
            elif tokens [k] == ")" or tokens [k] == "]" or \
                    tokens [k] == "]":
                bracket_depth -= 1
            if tokens[k] == "and" or tokens[k] == "in" or \
                    tokens[k] == "or" or tokens[k] == "+" or \
                    tokens[k] == "-" or \
                    tokens[k] == "%" or tokens[k] == "*" or \
                    tokens[k] == "/" or tokens[k] == "|" or \
                    tokens[k] == "~":
                if bracket_depth == 0:
                    return None
            k += 1

        # Put together expression:
        assert(CodeGenerator.is_balanced(tokens[:last_index]))
        assert(CodeGenerator.is_balanced(tokens[last_index + 1:-1]))
        prev_tokens = CodeGenerator._translate_tokens_for_dict_indexing(
            tokens[:last_index]) or tokens[:last_index]
        return ["(", "_wobbly_key_dict_handler", "("] + prev_tokens +\
            [","] + tokens[last_index + 1:-1] + [")", ")"]

    @staticmethod
    def get_end_of_plus_op_argument(tokens):
        bracket_depth = 0
        quit_if_no_continuation = False
        i = 0
        while i < len(tokens):
            if (tokens[i] == "[" or tokens[i] == "(" or tokens[i] == "{"
                    or tokens[i] == "{<"):
                quit_if_no_continuation = False
                bracket_depth += 1
            elif (tokens[i] == "]" or tokens[i] == ")" or tokens[i] == "}"
                    or tokens[i] == ">}"):
                quit_if_no_continuation = False
                bracket_depth -= 1
                if (bracket_depth < 0):
                    return i
                if bracket_depth == 0:
                    quit_if_no_continuation = True
                    i += 1
                    continue
            elif (tokens [i] == "*" or tokens [i] == "/"
                    or tokens [i] == "&" or tokens [i] == "^" or
                    tokens [i] == "~" or tokens [i] == "|"): # bind more closely
                quit_if_no_continuation = False
                i += 1
                continue
            elif tokens [i] == ".":  # regular continuation
                quit_if_no_continuation = False
                i += 1
                continue
            elif (tokens [i] == "," or tokens [i] == "\n" or
                    tokens [i] == "+" or tokens [i] == "-" or
                    tokens [i] == ":" or tokens [i] == "=" or
                    tokens [i] == ">" or tokens [i] == "<" or
                    tokens [i] == "in" or tokens [i] == "and" or
                    tokens [i] == "or"
                    ) and bracket_depth == 0:
                quit_if_no_continuation = False
                return i
            elif quit_if_no_continuation:
                return i
            i += 1
        return i

    def _parse_sub_tokens(self, tokens, inline=False):
        sub_generator = CodeGenerator(tokens,
            global_state=self.global_state, pretty=self.pretty,
            is_inline=inline,
            indent=self._indent)
        return sub_generator()

    def _is_global_scope(self):
        if self.global_state["block_level"] <= self.global_state[
                "most_inner_module_block"] and not self.inline:
            return True
        return False

    def handle_token(self, tokens, history):
        token = tokens[0]
        future = list(tokens)
        self.history = list(history)

        # "if", "elseif", "else" blocks:
        if token == "if" or token == "elseif" or token == "else":
            i = 1
            if token != "else":
                cant_end_here = True
                bracket_depth = 0
                while tokens[i] != "{" or bracket_depth > 0 or cant_end_here:
                    if (tokens[i] == "{" or tokens[i] == "(" or
                            tokens[i] == "["):
                        bracket_depth += 1
                    elif (tokens[i] == "}" or tokens[i] == ")" or
                            tokens[i] == "]"):
                        bracket_depth -= 1
                        cant_end_here = False
                        assert(bracket_depth >= 0),\
                            ("bracket_depth got negative at: " +
                            str(tokens[i:i+10]))
                    else:
                        if (tokens[i] == "=" or tokens[i] == ">" or
                                tokens[i] == ">" or tokens[i] == "not" or
                                tokens[i] == "!" or
                                tokens[i] == "and" or tokens[i] == "or"):
                            cant_end_here = True
                        else:
                            cant_end_here = False
                    i += 1
                    assert (i < len(tokens)),\
                        ("must find an end to '" + token +
                        "' expression: " + str(future[:10]) + "...")
            if token == "elseif":
                token = "elif"
            if token != "else":
                # Token condition:
                if_start = token + " " + self._parse_sub_tokens(
                    [token for token in tokens[1:i] if token.strip() != ""],
                    inline=True).replace("\n", " ").strip()
            else:
                if_start = "else"

            # Get end of code inside conditional block:
            assert(tokens[i] == "{"), "expected '{', got: " +\
                str(tokens[i]) + ", in block started by '" +\
                str(tokens[:5]) + "'"
            end_index = self.get_end_of_same_depth(
                tokens[i + 1:]) + i + 1
            assert(end_index < len(tokens)), (
                "unterminated block started by " +
                "'" + str(token) + "', with those contents " +
                "remaining: " + str(tokens[i:]))
            self._indent += 1
            code = "\n" + self.indent + self._parse_sub_tokens(
                tokens[i + 1:end_index],
                inline=False).lstrip()
            self._indent -= 1
            assert(end_index < len(tokens) and tokens[end_index] == "}")

            return (if_start + ":\n" + code +
                "\n" + self.indent + "    pass\n" + self.indent, end_index)

        # {< ... >} set constructor:
        if tokens[0] == "{<":
            end_index = self.get_end_of_bracket(
                tokens)
            code = self._parse_sub_tokens(
                tokens[1:end_index - 1], inline=True)
            assert(end_index <= len(tokens) and tokens[end_index - 1] == ">}")
            return ("set([" + code + "])", end_index - 1)

        # Process l.appendAll(x) to l + x
        if (tokens[0] == "." and len(tokens) > 2 and tokens[1] == "appendAll"\
                and tokens[2] == "("):
            end_index = self.get_end_of_bracket(tokens[2:]) + 2
            assert(end_index <= len(tokens) and tokens[end_index - 1] == ")")
            self._indent += 1
            code = ""
            if end_index > 4:
                code = "\n" + self.indent + self._parse_sub_tokens(
                    tokens[3:end_index - 1],
                    inline=False).lstrip()
            self._indent -= 1
            return (" += (" + str(code) + ")", end_index - 1)

        

        # Process .length to len() and .sort() to sorted():
        if (ord(token[0]) >= ord("a") and ord(token[0]) <= ord("z")) or\
                token[0] == "_" or token[0] == "'" or token[0] == "\"":
            expr_len = 1
            while expr_len < len(tokens):
                if (tokens[expr_len] == "." and expr_len < len(tokens) - 1
                        and tokens[expr_len + 1] != "length" and
                        tokens[expr_len + 1] != "sorted"):
                    expr_len += 2
                    continue
                if tokens[expr_len] == "(" or tokens[expr_len] == "[":
                    end_index = self.get_end_of_bracket(
                        tokens[expr_len:]) + expr_len
                    expr_len = end_index
                    continue
                break
            if expr_len < len(tokens) - 1:
                if tokens[expr_len] == "." and (tokens[
                        expr_len + 1] == "length" or
                        (tokens[expr_len + 1] == "sorted" and
                        expr_len < len(tokens) - 3 and
                        tokens[expr_len + 2] == "(" and
                        tokens[expr_len + 3] == ")")):
                    inner_code = self._parse_sub_tokens(
                        tokens[0:expr_len], inline=True)
                    if tokens[expr_len + 1] == "length":
                        return ("(\n" +\
                            "\n" + self.indent + "len(" + inner_code + "))",
                            expr_len + 1)
                    else:
                        return ("(\n" +\
                            "\n" + self.indent + "_ChiCraftList(sorted(" +
                            inner_code + ")))",
                            expr_len + 3)
        elif token == "(":
            end_index = self.get_end_of_bracket(tokens[0:]) + 0
            if end_index < len(tokens) - 1:
                assert(tokens[end_index - 1] == ")"), \
                    "end_index with self.get_end_of_bracket from " +\
                    str(tokens[:5]) + " should end with ')' or " +\
                    "end of file, but ends with " + \
                    str(tokens[end_index - 1])
                if tokens[end_index] == "." and tokens[
                        end_index + 1] == "length":
                    return ("(len(" + self._parse_sub_tokens(
                        tokens[1:end_index - 1], inline=True) + "))",
                        end_index + 1)

        # Handle "+=" additions:
        if token[0] == "+" and len(tokens) > 1 and \
                tokens[1] == "=" and len(history) > 1:
            i = 2
            while i < len(tokens) and tokens[i] != "\n":
                i += 1
            end_index = i

            # Assigned thing before +=
            i = len(history) - 1
            while i >= 0 and history[i] != "\n":
                i -= 1
            assign_tokens = history[i + 1:len(history) - 1]
            assert(len(assign_tokens) > 0)
            self._indent += 1
            assign_code = self._parse_sub_tokens(
                assign_tokens, inline=True)
            self._indent -= 1

            # Code after +=
            self._indent += 1
            code = self._parse_sub_tokens(
                tokens[2:end_index], inline=True)
            self._indent -= 1
            return ("= _wobbly_addition_handler(" +
                assign_code + "," + code + ")", end_index - 1)

        # "class" definitions:
        if token == "class":
            assert(not self.global_state["inside_class"])
            derived_from = "_ChiCraftBaseObj"
            i = 2
            if tokens[2] == "extends":
                i = 3
                while tokens[i] == "\n":
                    i += 1
                derived_from = tokens[i]
                i += 1
                while i + 1 < len(tokens) and tokens[i] == ".":
                    derived_from += tokens[i] + tokens[i + 1]
                    i += 2
            while i < len(tokens) and tokens[i] == "\n":
                i += 1
            assert(tokens[i] == "{"), \
                ("expecting '{' following 'class' definition: " + str(tokens[:10]))
            i += 1

            # Collect all var defs in the beginning of the class definition:
            self.global_state["class_vardefs"] = {}
            while True:
                if tokens[i] == "\n" or tokens[i].startswith("#"):
                    i += 1
                    continue
                if tokens[i] == "let" and i < len(tokens) + 2:
                    var_name = tokens[i + 1]
                    if tokens[i + 2] == "=":
                        end_index = self.get_end_of_bracket(
                            tokens[i+3:]) + i + 3
                        self.global_state["class_vardefs"][var_name] = (
                            self._parse_sub_tokens(tokens[i+3:end_index],
                                inline=True).strip())
                        i = end_index
                        continue
                    else:
                        self.global_state["class_vardefs"][var_name] = "None"
                        i += 2
                        continue
                if i > 0 and tokens[i - 1] == "\n":
                    i -= 1
                break

            # Get end of code inside class definition:
            end_index = self.get_end_of_same_depth(
                tokens[i:]) + i
            self._indent += 1
            assert(not self.global_state["inside_class"])
            before_class_state = self.global_state["inside_class"]
            self.global_state["inside_class"] = True
            self.global_state["init_found"] = False
            code = self._parse_sub_tokens(
                tokens[i:end_index], inline=False)

            # Append default __init__ function if none:
            if not self.global_state["init_found"]:
                code += "\n" + self.indent + "def __init__ (self, *args, **kwargs):"
                code += "\n" + self.indent + "    super().__init__(*args, **kwargs)"
                for var_name in self.global_state["class_vardefs"]:
                    code += "\n" + self.indent + "    self." + var_name
                    code += " = " + self.global_state["class_vardefs"][
                        var_name]
                code += "\n" + self.indent + "    pass\n"
            self.global_state["inside_class"] = before_class_state
            del(self.global_state["class_vardefs"])
            self._indent -= 1

            # __repr__ code:
            tostr_code = self.indent + "    def __repr__(self):\n"
            tostr_code += self.indent + ("        if hasattr(self, " +
                "\"tostring\"):\n")
            tostr_code += self.indent + "            return self.tostring()\n"
            tostr_code += self.indent + "        return super().__repr__()\n"
            code = tostr_code + code

            assert(not self.global_state["inside_class"])
            return ("class " + tokens[1] + "(" + derived_from + "):\n" +
                code + "\n" + self.indent + "    pass\n", end_index)

        # Turn "super" into "super()":
        if token == "super":
            if len(tokens) >= 3 and tokens[1] == "." and \
                    tokens[2] == "init":
                return ("super().__init__", 2)
            return ("super()", 0)

        # "assert(..)" reformatting
        if token == "assert" and len(tokens) > 2 and tokens[1] == "(":
            end_index = self.get_end_of_bracket(
                tokens[2:],
                end_at_negative_index=True) + 2
            first_param = self._parse_sub_tokens(tokens[2:end_index],
                inline=True)
            if tokens[end_index] == ",":
                first_end = end_index
                end_index = self.get_end_of_bracket(
                    tokens[first_end+1:],
                    end_at_negative_index=True) + first_end + 1
                second_param = self._parse_sub_tokens(
                    tokens[first_end+1:end_index],
                    inline=True)
            else:
                second_param = None 
            assert(tokens[end_index] == ")")
            t = "assert " + str(first_param).strip()
            if second_param != None:
                t += ", " + str(second_param).strip()
            return (t, end_index)

        # Some simple misc replacements:
        if token == "true":
            return ("True", 0)
        elif token == "false":
            return ("False", 0)
        elif token == "math" and len(future) >= 3 and \
                tokens[1] == "." and (tokens [2] == "max" or
                tokens[2] == "min"):
            return (future [2], 2)
        elif token == "math" and len(future) >= 3 and \
                tokens[1] == "." and tokens[2] == "power":
            return ("math.pow", 2)
        elif token == "multiarg":
            return ("*", 0)
        elif token == "rtrim" and len(history) >= 2 and history[-2] == ".":
            return ("rstrip", 0)
        elif token == "ltrim" and len(history) >= 2 and history[-2] == ".":
            return ("lstrip", 0)
        elif token == "trim" and len(history) >= 2 and history[-2] == ".":
            return ("strip", 0)
        elif token == "null":
            return ("None", 0)
        elif token == "throw":
            return ("raise", 0)
        elif token == "TypeException":
            return ("TypeError", 0)
        elif token == "IOException":
            return ("OSError", 0)
        elif token == "RuntimeException":
            return ("RuntimeError", 0)
        elif token == "PermissionException":
            return ("PermissionError", 0)
        elif token == "hasmember":
            return ("hasattr", 0)
        elif token == "remove_by_index":
            return ("pop", 0)
        elif token == "NotImplementedException":
            return ("NotImplementedError", 0)
        elif (token == "pythonlang" and len(tokens) >= 3 and tokens[1] == "."
                and tokens[2] == "open"):
            return ("_pythonlang_builtin_open", 2)
        elif token == "file":
            if (len(self.history) < 2 or (self.history[-2] != "," and
                    self.history[-2] != "(") or len(future) < 2 or
                    future[1] != "="):  # check if not keyword arg
                return ("_var_named_file", 0)
        elif token == "str":
            if (len(self.history) < 2 or (self.history[-2] != "," and
                    self.history[-2] != "(") or len(future) < 2 or
                    future[1] != "="):  # check if not keyword arg
                return ("_var_named_str", 0)
        elif token == "len":
            if (len(self.history) < 2 or (self.history[-2] != "," and
                    self.history[-2] != "(") or len(future) < 2 or
                    future[1] != "="):  # check if not keyword arg
                return ("_var_named_len", 0)

        # "try ... catch" statement:
        if token == "try":
            # Get to corresponding "catch":
            assert(tokens[1] == "{")
            end_index = self.get_end_of_same_depth(tokens[2:]) + 2
            assert(tokens[end_index] == "}" and
                (tokens[end_index + 1] == "catch"
                or tokens[end_index + 1] == "finally"))
            no_catch = False
            if tokens[end_index + 1] == "finally":
                no_catch = True

            # Get code: "try { .. HERE .. } catch x { .. }"
            self._indent += 1
            code_first = self._parse_sub_tokens(
                tokens[2:end_index], inline=False)
            self._indent -= 1

            # Parse "catch .. [as ..]" part
            if not no_catch:
                i = end_index
                assert(tokens[end_index] == "}")
                i += 1
                assert(tokens[i] == "catch")
                i += 1

                # Parse: catch PARSINGTHISHERE as ..
                caught = [tokens[i]]
                while i < len(tokens) - 2 and tokens[i + 1] == ".":
                    caught[-1] += "." + tokens[i + 2]
                    i += 2

                named_as = None

                # Parse: catch v1,PARSINGTHISHERE as .. (multiple caught items)
                while i < len(tokens) - 2 and tokens[i + 1] == ",":
                    caught += tokens[i + 2]
                    i += 2
                    while i < len(tokens) - 2 and tokens[i + 1] == ".":
                        caught[-1] += "." + tokens[i + 2]
                        i += 2
                i += 1

                # Parse: catch myerr as PARSINGTHISHERE
                if i < len(tokens) - 2 and tokens[i] == "as":
                    named_as = tokens[i + 1]
                    i += 2

                # Put it together:
                middle = ("\n" + self.indent + "except (" + ",".join(
                    [self._parse_sub_tokens([item], inline=True).strip()
                        for item in caught]) +
                    ")")
                if named_as != None:
                    middle += "as " + named_as

                # Check for '{' after catch:
                assert(tokens[i] == "{"),\
                    "expected '{' but got " + str(tokens[i])
                i += 1

                # Find ending "}":
                end_index = self.get_end_of_same_depth(tokens[i:]) + i
                assert(end_index < len(tokens) and tokens[end_index] == "}")

                # Get code: "try { .. } catch x { .. HERE .. }"
                self._indent += 1
                code_second = self._parse_sub_tokens(
                    tokens[i:end_index], inline=False)
                self._indent -= 1

                code_finally = ""

                # See if we have a finally block at the end:
                if end_index < len(tokens) and \
                        tokens[end_index + 1] == "finally":
                    assert(tokens[end_index + 2] == "{")
                    i = end_index + 3
                    end_index = self.get_end_of_same_depth(tokens[i:]) + i
                    assert(end_index < len(tokens) and
                        tokens[end_index] == "}")
                    self._indent += 1
                    code_finally = self._parse_sub_tokens(
                        tokens[i:end_index], inline=False)
                    self._indent -= 1

                return ("try:\n" + self.indent + code_first +
                    middle + ":\n" + self.indent + code_second +
                    "\n" + self.indent + "    pass\n" +
                    self.indent + "finally:\n" + code_finally + "\n" +
                    self.indent + "    pass\n", end_index)
            else:
                # Extract finally block:
                assert(tokens[end_index + 2] == "{")
                i = end_index + 3
                end_index = self.get_end_of_same_depth(tokens[i:]) + i
                assert(end_index < len(tokens) and
                        tokens[end_index] == "}")
                self._indent += 1
                code_finally = self._parse_sub_tokens(
                        tokens[i:end_index], inline=True)
                self._indent -= 1

                return ("try:\n" + self.indent + code_first + "\n" +
                    self.indent + "finally:\n" + code_finally + "\n" +
                    self.indent + "    pass\n", end_index)

        # "func" definitions:
        if token == "func":
            # Get function parameters:
            assert(tokens[2] == "(" or tokens[2] == "{")
            param_tokens = []
            if tokens[2] == "(":
                end_index_bracket = self.get_end_of_bracket(tokens[2:]) + 2
                assert(end_index_bracket < len(tokens)), (
                    "unterminated function param list started by " +
                    "'" + str(token) + "', with those contents " +
                    "remaining: " + str(tokens[2:]))
                assert(tokens[end_index_bracket - 1] == ")")
                i = end_index_bracket
                param_tokens = tokens[2:end_index_bracket]
            else:
                i = 2
            while i < len(tokens) and tokens[i] == "\n":
                i += 1
            assert(tokens[i] == "{"), \
                "EXPECTED '{' AFTER FUNC CALL. TOKEN EXTRACT: " +\
                str(tokens[i-5:i+3]) + ", ENTIRE RANGE: " + str(tokens[:i+1])
            i += 1
            params = "()"
            if len(param_tokens) > 0:
                params = self._parse_sub_tokens(
                    param_tokens, inline=True)
            if self.global_state["inside_class"]:
                params = "(self, " + params[1:]

            # Function name:
            func_vardef_code = ""
            func_name = tokens[1]
            if self.global_state["inside_class"] and func_name == "init":
                func_name = "__init__"
                if "class_vardefs" in self.global_state:
                    for k in self.global_state["class_vardefs"]:
                        func_vardef_code += "\n" + self.indent + \
                            "    self." +\
                            k + " = " +\
                            self.global_state["class_vardefs"][k]
                    func_vardef_code += "\n"
                self.global_state["init_found"] = True

            # Global vars:
            func_globvar_code = ""
            for var_name in self.global_state["known_global_vars"]:
                func_globvar_code += "\n" + self.indent + \
                    "    global " + var_name
            func_globvar_code += "\n"

            # Get end of inner code:
            end_index = self.get_end_of_same_depth(
                tokens[i:]) + i
            assert(end_index < len(tokens) and tokens[end_index] == "}")

            # Generate inner code with higher indent:
            self._indent += 1
            result = "\n" + self.indent + self._parse_sub_tokens(
                tokens[i:end_index], inline=False).lstrip()
            self._indent -= 1
            return ("def " + func_name + " " +
                params + ":\n" + func_vardef_code +
                func_globvar_code + result +
                "\n" + self.indent + "    pass\n",
                end_index)

        # "for", "while" loops:
        if token == "for" or token == "while":
            i = 1

            # Find end of loop condition:
            cant_end_here = True
            bracket_depth = 0
            while tokens[i] != "{" or bracket_depth > 0 or cant_end_here:
                if (tokens[i] == "{" or tokens[i] == "(" or
                        tokens[i] == "["):
                    bracket_depth += 1
                elif (tokens[i] == "}" or tokens[i] == ")" or
                        tokens[i] == "]"):
                    bracket_depth -= 1
                    cant_end_here = False
                    assert(bracket_depth >= 0)
                else:
                    if (tokens[i] == "=" or tokens[i] == ">" or
                            tokens[i] == ">" or tokens[i] == "not" or
                            tokens[i] == "!" or
                            tokens[i] == "and" or tokens[i] == "or"):
                        cant_end_here = True
                    else:
                        cant_end_here = False
                i += 1
                assert (i < len(tokens)),\
                    ("must find an end to '" + token +
                    "' conditional expression: " + str(future[:15]) + "...")
            i += 1  # Skip over "{"
 
            # Get end of code inside loop:
            end_index = self.get_end_of_same_depth(
                tokens[i:]) + i
            self._indent += 1
            code = self._parse_sub_tokens(
                tokens[i:end_index], inline=False)
            self._indent -= 1

            return (token + " " + self._parse_sub_tokens(
                [token for token in tokens[1:i-1] if token.strip() != ""],
                inline=True).strip().replace("\n", " ")
                + ":\n" +
                code + "\n" + self.indent + "    pass\n", end_index) 

        # Indentation:
        if token == "\n":
            return ("\n" + self.indent, 0)

        # Ignore "let", handle empty assignments:
        if token == "let":
            if len(tokens) > 3 and tokens[2] == "=":
                return ("", 0)
            else:
                return (tokens [1] + " = None", 1)

        # Handle "tostring" call:
        if token == "tostring" and (len(self.history) < 2 or \
                self.history [-2] != "function") and\
                future[1] != "=":
            assert(future[1] == "(")
            end_index = self.get_end_of_bracket(tokens[1:]) + 1
            assert(tokens[end_index - 1] == ")")
            return ("_wobbly_tostring(" + self._parse_sub_tokens(
                tokens[1:end_index], inline=True) + ")", end_index - 1)

        # Handle xxx.copy() on lists and dictionaries:
        #if len(future) >= 5 and future[1] == "." and future[2] == "copy" and \
        #        future[3] == "(" and future[4] == ")" and history[-2] != ".":
        #    return ("((typeof(" + token + ")!=\"undefined\"&&" +\
        #        "typeof(" + token + ".copy)!=\"undefined\")?(" + token +\
        #    ".copy()):((function(){var _oref=" + token + ";" +\
        #    "if(typeof(_oref)==\"undefined\"||_oref==null)return _oref;"
        #    "var _newo=new Object();" +\
        #    "for(var k in _oref){if(!_oref.hasOwnProperty(k)){continue;}" +\
        #    "_newo[k]=_oref[k];}return _newo;})()))", 4)

        # Translate X[Y] to _wobbly_key_dict_handler(X, Y):
        end_index = self.get_end_of_expr (tokens)
        if end_index > 0 and end_index < len(tokens) - 1 and \
                tokens[end_index - 1] == "]":
            # Make sure this is not followed by an assignment, since we
            # can't translate left-hand expression like this:
            if end_index > len(tokens) - 2 or ((tokens[end_index] != "=" or \
                    tokens[end_index + 1] == "=") and
                    ((not tokens[end_index] in ["+", "-", "*",
                        "/", "%", "|", "^", "&"]) or
                        tokens[end_index + 1] != "=")):
                # Translate the tokens to _wobbly_key_dict_handler func call,
                # and recursively parse over them again:
                result = self._translate_tokens_for_dict_indexing(
                    tokens[:end_index])
                if result != None:
                    self._indent += 1
                    code = self._parse_sub_tokens(
                        result, inline=True)
                    self._indent -= 1
                    return (code, end_index - 1)

        # Handle + allowing implicit int conversion:
        no_int_converse = set (["let", "func",
            "if", "while", "for",
            "try", "catch", "(", ")", "[", "]",
            "{", "}", "{<", ">}", "+", "-", "/", "*",
            "^", "~", "|", "&", ":", ",",
            "and", "or", "multiarg"
             ])
        if (not (token in no_int_converse) and
                ((ord(token [0]) >= ord("a") and
                ord(token [0]) <= ord("z")) or
                (ord(token [0]) >= ord("A") and
                ord(token [0]) <= ord("Z")) or
                (ord(token [0]) >= ord("0") and
                ord(token [0]) <= ord("9")) or
                token [0] == "_" or
                token [0] == "(" or
                token [0] == "{<" or
                token [0] == "[" or
                token [0] == "{" or
                token [0] == "'" or
                token [0] == "\"")
                ) and (len(self.history) > 1 or
                self.inline):
            arguments = []
            end_index = self.get_end_of_plus_op_argument(tokens)
            if (len(history) <= 1 or (self.history [-2] == "(" or
                    self.history [-2] == "[" or
                    self.history [-2] == "{" or
                    self.history [-2] == "," or
                    self.history [-2] == ":" or
                    self.history [-2] == "="
                    )) and \
                    (end_index < len(tokens) and
                    tokens [end_index] == "+"): # This is a + op start!
                arguments = [(0, end_index)]
                # Get further arguments:
                while True:
                    next_arg_start = end_index + 1
                    end_index = self.get_end_of_plus_op_argument(tokens[
                        next_arg_start:]) + next_arg_start
                    arguments.append ((next_arg_start, end_index))
                    if end_index >= len(tokens) or \
                            tokens [end_index] != "+":
                        break

                # Output complete expression:
                self._indent += 1
                code = ""
                first = True
                for argument in arguments:
                    if first:
                        first = False
                    else:
                        code += ","
                    code += self._parse_sub_tokens(
                        tokens[argument[0]:argument[1]], inline=True)
                self._indent -= 1
                return ("_wobbly_addition_handler(" +
                    code + ")", end_index - 1)

        # Return token:
        self._prev_token = token
        return (token, 0)

    def __call__(self):
        s = ""
        tokens = list(self.tokenizer)
        i = 0
        while i < len(tokens):
            if tokens[i] in set([
                    "if", "func", "class", "while", "let",
                    "for"]) and i > 0:
                assert(i == 0 or tokens[i - 1] == "\n"), \
                    ("valid token stream needs line break before " +
                    "'" + str(tokens[i]) +
                    "' - inline '" + str(tokens[i]) +
                    "'s are not supported by " +
                    "baseline runner. (Also please note the baseline " +
                    "runner generally doesn't support putting multiple " +
                    "statements in a single line, despite this being " +
                    "valid in regular tethercode - keep in mind this is " +
                    "only a quite limited implementation/subset) " +
                    "Previous tokens are: " + str(tokens[max(0, i - 5):i]) +
                    " (we are at index " + str(i) + ")")
            (t, skip_tokens) = self.handle_token(tokens[i:], tokens[:i+1])
            if t == None or len(t) == 0:
                i += (1 + skip_tokens)
                continue
            non_space_chars = [ ")", "]", "}", ">", "=",
                ".", "\n", "[", "+", "<", " ", "*", "-", "\r" ]
            non_space = False
            for char in non_space_chars:
                if s.endswith(char):
                    non_space = True
                    break
            if t.startswith(".") or t.startswith("(") or\
                    t.startswith("[") or t.startswith("=") or\
                    t.startswith(" ") or t.startswith("\n"):
                non_space = True
            if not non_space:
                s += " "
            s += t
            i += (1 + skip_tokens)
        if len(self.prepend_code) > 0:
            return self.prepend_code + "\n" + s
        return s

class ProgramGenerator(object):
    def __init__(self, s, full_path, verbose=True, no_main=False,
            packages_dir="tc_packages"):
        self.temp_path = tempfile.mkdtemp(prefix="wobbly-baseline-tmp-")
        self.contents = s
        self.verbose = verbose
        self.no_main = no_main
        self.packages_dir = packages_dir

        if not os.path.isabs(self.packages_dir):
            self.packages_dir = os.path.join(
                os.path.abspath(os.path.dirname(full_path)),
                self.packages_dir)
            assert(os.path.exists(self.packages_dir)),\
                "missing packages dir: " + str(self.packages_dir) +\
                " (with full path: " + str(full_path) + ", baseline " +\
                "overall args: " + str(sys.argv) + ")"

        self.full_path = full_path
        if not os.path.isabs(full_path):
            self.full_path = os.path.abspath(full_path)
        self.input_source_dir = os.path.dirname(full_path)
        if self.input_source_dir == "":
            self.input_source_dir = os.getcwd()
        assert(os.path.exists(self.input_source_dir) and
            os.path.isdir(self.input_source_dir)), \
            "expected to be dir but isn't: " + str(self.input_source_dir)
        self.filename = os.path.basename(self.full_path)

    def _run(self):
        # Process all imports, translate wobbly code and write it to disk:
        self.file_list = PreImportParser(self.contents, self.filename,
            base_paths=[self.full_path],
            packages_dir=self.packages_dir,
            full_file_path=self.full_path)()
        for fname in self.file_list:
            if self.verbose:
                print("Parsing and translating file: " + fname)

            if not fname.endswith(".tc"):
                raise RuntimeError("invalid source code file " +
                    "with filename not ending with .tc")

            # Final path for the file in temp dir:
            local_target_path = os.path.join(
                self.temp_path, fname)

            # Make sure all subdirectories exist:
            subdirs_path = os.path.normpath(os.path.dirname(fname))
            if len(subdirs_path) > 0 and subdirs_path != ".":
                subdir_parts = subdirs_path.split(
                    os.path.sep)
                temp_path = self.temp_path
                for part in subdir_parts:
                    temp_path = os.path.join(temp_path, "_tethercode_imported_" +
                        part)
                    if self.verbose:
                        print("Creating directory: " + temp_path)
                    if not os.path.exists(temp_path):
                        os.mkdir(temp_path)
                        with open(os.path.join(temp_path, "__init__.py"),
                                "w") as f:
                            pass
                new_fname = os.path.join((os.path.sep).join(
                    ["_tethercode_imported_" + part for part in subdir_parts]),\
                    os.path.basename(fname))
                if new_fname.startswith(os.path.sep):
                    new_fname = new_fname[len(os.path.sep):]
                local_target_path = os.path.join(self.temp_path, new_fname)

            # Write file:
            write_path = local_target_path[:-3] + ".py"
            append_code = ""
            if not self.filename == fname:
                write_path = os.path.join(self.temp_path,
                    os.path.dirname(local_target_path),
                    "_tethercode_imported_" + os.path.basename(local_target_path))
                write_path = write_path[:-3] + ".py"
                append_code = "\ntry:\n    on_import()\n" +\
                    "except NameError:\n    pass\n"
            elif not self.no_main:
                append_code = "\nmain()"
            with open(write_path, "w") as f:
                # Generate translated code:
                translated_code = CodeGenerator(
                    AssignmentOperatorReplacementTokenizer(
                    ListDictTransformTokenizer(
                        LinebreakCleanupTokenizer(Tokenizer(
                            PrecompileCheckParser(
                                self.file_list[fname][0]
                            , fname,
                            base_dir=os.path.dirname(os.path.normpath(
                            self.file_list[fname][1])),
                            verbose=self.verbose)()
                        ))
                    ))(),
                    global_state=None,
                    pretty=True,
                    is_inline=False, indent=0)() + append_code
                
                # Write code to file:
                f.write(translated_code)
            if self.verbose:
                print("File written: " + write_path)

        self.temp_run_path = os.path.join(
            self.temp_path, self.filename)[:-3] + ".py"

    def __del__(self):
        try:
            if self.temp_path != None:
                if self.verbose:
                    print("Cleaning up...")
                if os.path.exists(self.temp_path):
                    shutil.rmtree(self.temp_path)
        except Exception as e:
            print("baseline-runner.py: warning: cleanup failed: " +
                str(e))

    def __call__(self):
        self._run()
        return self.temp_run_path

def sucks_warning():
    print("")
    print("-------------- PLEASE READ BEFORE USE ---------------",
        file=sys.stderr)
    print("WARNING: This translator for tethercode is VERY BAD.",
        file=sys.stderr)
    print("         The language implementation is INCOMPLETE.",
        file=sys.stderr)
    print("         It makes INTRANSPARENT ASSUMPTIONS about the code,\n" +\
          "         which are hard to adhere to without in-depth knowledge.",
        file=sys.stderr)
    print("         The error handling is MISSING and/or atrocious.",
        file=sys.stderr)
    print("This is only meant as an absolute base bootstrap to get the\n"+\
        "actual, properly written compiler running.", file=sys.stderr)
    print("You have been warned!", file=sys.stderr)
    print("------------------ End of warning. ------------------",
        file=sys.stderr)
argparser = argparse.ArgumentParser(description=\
    "tethercode baseline runner")
argparser.add_argument("-V",
    help="Show version", default=False,
    dest="show_version", action="store_true")
argparser.add_argument("-v",
    help="Verbose output", default=False,
    dest="verbose", action="store_true")
argparser.add_argument("--compile-to-file", "-o", nargs="?",
    help="Output file for static conversion. If this "
    "option is not specified,"
    " the default is to run the program directly", default=None,
    dest="output")
argparser.add_argument("--packages-dir", nargs="?",
    help="The packages directory from where files can be " +
        "imported. Defaults to tc_packages/",
    dest="packages_dir", default="tc_packages")
argparser.add_argument("--pretty",
    help="Generate prettier code for better debugging",
    default=False, dest="pretty", action="store_true")
argparser.add_argument("--no-cleanup",
    help="Don't clear up temporary files in system temp folder" +
        "after termination", default=True, dest="cleanup",
        action="store_false")
argparser.add_argument("--node-debugger",
    help="Run with the node debugger, and start paused",
    default=False, dest="node_debugger", action="store_true")
argparser.add_argument("--no-main",
    help="Don't add automatic main call to end of script",
    default=False, dest="no_main", action="store_true")
argparser.add_argument("file",
    help="tethercode language code file to be processed")
argparser.add_argument("arguments", nargs="*",
    help="Arguments to be passed to the tethercode program")
if len(sys.argv) < 2:
    argparser.print_help()
    sucks_warning()
    sys.exit(1)
def up_to_guaranteed_file_arg():
    l = []
    i = 1
    while i < len(sys.argv):
        if sys.argv[i][0] != "-":
            break
        l.append(sys.argv[i])
        i += 1
    return l

# See if this definitely has a --help or --version flag and process it:
gargs = up_to_guaranteed_file_arg()
if "-V" in gargs or "--version" in gargs:
    print(argparser.description)
    print("  Program version:    " + PROGRAM_VERSION)
    print("  Python version:     " + str(sys.version).replace("\n", " "))
    print("  Python executable:  " + sys.executable)
    sucks_warning()
    sys.exit(0)
if "-h" in gargs or "--help" in gargs or "-?" in gargs:
    argparser.print_help()
    sucks_warning()
    sys.exit(0)

# Get our args that are not meant for the called script itself:
our_args = []
prev_arg = ""
for arg in sys.argv[1:]:
    if not arg.startswith("-") and not prev_arg == "-o" and \
            not prev_arg == "--compile-to-file" and \
            not prev_arg == "--packages-dir":
        if sys.argv[len(our_args) + 1] == "--":
            our_args.append("--")
        break
    our_args.append (arg)
    prev_arg = arg
remaining_args = sys.argv[1 + len(our_args):]

# Parse our args:
args = argparser.parse_args(our_args + remaining_args[:1])

# Set various options:
run_program = False
output_path = args.output
output_temp_dir_path = None
if output_path == None:
    run_program = True
fcontents = ""
fpath = args.file
call_fpath = fpath

# Read entire script into memory:
with open(fpath, "r") as f:
    fcontents = f.read()

# If running the program, shut up about compilation details:
if run_program or True:
    logging.getLogger().setLevel(logging.WARNING)

# Generate code:
if args.verbose:
    print("baseline-runner.py: verbose: " +
        "baseline runner starting code generation...")
    print("baseline-runner.py: verbose: " +
        "packages_dir is: " + str(
        args.packages_dir))
programgen = ProgramGenerator(fcontents, fpath,
    verbose=args.verbose, no_main=args.no_main,
    packages_dir=args.packages_dir)
try:
    run_path = programgen()
    if output_path != None:
        shutil.copytree(os.path.dirname(run_path), output_path)
except Exception as e:
    if not args.cleanup:
        programgen.temp_path = None
    del(programgen)
    raise e

# Run if the user wants us to:
if run_program:
    try:
        if platform.system() == "windows":
            # Call it directly:
            exit_code = subprocess.call([sys.executable,
                run_path] + remaining_args[1:])
            sys.exit(exit_code)
        else:
            # Use a pty:
            file_args = [run_path]
            assert(os.path.exists(run_path))
            full_args = (file_args + [call_fpath] +
                remaining_args[1:])
            assert(os.path.exists(sys.executable))
            prog = sys.executable
            exit_code = subprocess.call([prog, "-c",
                "import pty\nimport sys\n" +
                "exit_code = pty.spawn(sys.argv[1:])\n" +
                "final = ((int(exit_code) & (255 << 8)) >> 8)\n" +
                "sys.exit(final)\n",
                sys.executable] + full_args)
    finally:
        if not args.cleanup:
            programgen.temp_path = None
        del(programgen)
    sys.exit(int(exit_code))

