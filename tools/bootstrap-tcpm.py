#!/usr/bin/python3

import os
import platform
import shutil
import subprocess
import sys

def get_tcpm():
    tcpm_url = "https://gitlab.com/tethercode/tcpm"

    repo_base = os.path.normpath(os.path.join(
        os.path.abspath(os.path.dirname(
            __file__)),
        ".."))
    module_path = os.path.normpath(os.path.join(
        repo_base,
        "tc_packages"))
    bin_path = os.path.join(module_path, "bin")
    local_tcpm_path = os.path.join(bin_path, "tcpm")
    baseline_path = os.path.normpath(os.path.join(
        os.path.abspath(os.path.dirname(__file__)),
        "..",
        "tools", "baseline-runner.py"))
    if platform.system().lower() == "windows":
        local_tcpm_path += ".exe"

    have_global_install = True
    try:
        subprocess.check_output(["tcpm", "--help"])
    except (subprocess.CalledProcessError, OSError):
        have_global_install = False

    if have_global_install:
        return "tcpm"

    # Download and locally install tcpm if not present:
    if not os.path.exists(local_tcpm_path):
        print("tools/baseline-tcpm.py: info: " +
            "tcpm not locally present. Bootstrapping tcpm...")
        # Remove previous download if any:
        if os.path.exists(os.path.join(
                os.path.dirname(__file__),
                ".tcpm-cache")):
            shutil.rmtree(
                os.path.join(
                os.path.dirname(__file__),
                ".tcpm-cache"))

        # See if we can find tcpm locally as a neighbor directory to
        # this repository:
        tcpm_neighbor = os.path.normpath(os.path.abspath(
            os.path.join(repo_base, "..", "tcpm")))
        try:
            if not os.path.exists(tcpm_neighbor) or \
                    not os.path.isdir(tcpm_neighbor) or \
                    not os.path.exists(os.path.join(
                        tcpm_neighbor, "tcpm.ini")) or \
                    not os.path.exists(os.path.join(
                        tcpm_neighbor, "tcpm.tc")):
                tcpm_neighbor = None
        except OSError:
            tcpm_neighbor = None

        if tcpm_neighbor == None:
            # Obtain new download from latest git master:
            print("tools/baseline-tcpm.py: info: " +
                "downloading tcpm from repository " +
                "master to run bootstrap install...")
            try:
                subprocess.check_output(["git", "clone",
                    tcpm_url,
                    ".tcpm-cache"], cwd=os.path.abspath(
                    os.path.dirname(__file__)),
                    stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError:
                print("tools/baseline-tcpm.py: error: " +
                    "git checkout failed. Detailed " +
                    "error output: " + str(e), file=sys.stderr)
                sys.exit(1)
        else:
            # Copy neighboring tcpm dir:
            print("tools/baseline-tcpm.py: info: " +
                "neighbor checkout of tcpm detected at: " +
                tcpm_neighbor)
            print("tools/baseline-tcpm.py: info: " +
                "copying neighbor checkout into download " +
                "cache...")
            shutil.copytree(tcpm_neighbor,
                os.path.join(os.path.abspath(
                    os.path.dirname(__file__)),
                    ".tcpm-cache"))

        temp_tcpm_bin = os.path.abspath(os.path.join(
            os.path.dirname(__file__),
            ".tcpm-cache", "tcpm.tc"))
        tcpm_base = os.path.abspath(os.path.join(
            os.path.dirname(__file__),
            ".tcpm-cache"))

        # Make sure packages dir with stdlib is around:
        print("tools/baseline-tcpm.py: info: " +
            "copying local repo into package cache as " +
            "io.tethercode.stdlib...")
        if not os.path.exists(module_path):
            os.mkdir(module_path)
        if not os.path.exists(os.path.join(
                module_path, "cache")):
            os.mkdir(os.path.join(module_path, "cache"))
        if os.path.exists(os.path.join(module_path,
                "cache", "io.tethercode.stdlib")):
            shutil.rmtree(os.path.join(module_path,
                "cache", "io.tethercode.stdlib"))
        os.mkdir(os.path.join(module_path,
            "cache", "io.tethercode.stdlib"))
        for f in os.listdir(repo_base):
            if f in ["tc_packages", "tools",
                    "misc", ".git", "debug"] or \
                    f.startswith(".git"):
                continue
            fpath = os.path.join(repo_base, f)
            tgpath = os.path.join(module_path,
                "cache", "io.tethercode.stdlib", f)
            if os.path.isdir(fpath):
                shutil.copytree(fpath, tgpath)
            else:
                shutil.copy(fpath, tgpath)

        # Run tcpm to install stdlib:
        assert(not os.path.exists(
            os.path.join(repo_base, "tc_packages",
            "io.tethercode.stdlib", "tc_packages")))
        bootstrap_stdlib_cmd = [
            sys.executable,
            baseline_path,
            "--packages-dir", os.path.join(
                repo_base,
                "tc_packages", "cache"),
            temp_tcpm_bin,
            "-v", "--package-dir", module_path,
            "install", "--baseline-bootstrapped",
            "."
            ]
        print("tools/baseline-tcpm.py: info: " +
            "running stdlib bootstrap cmd: " +
            str(bootstrap_stdlib_cmd))
        result = subprocess.call(bootstrap_stdlib_cmd,
            cwd=repo_base)
        if result != 0:
            print("tools/baseline-tcpm.py: error: " +
                "stdlib failed to install itself.",
                file=sys.stderr)
            sys.exit(1)
        assert(not os.path.exists(
            os.path.join(repo_base, "tc_packages",
            "io.tethercode.stdlib", "tc_packages")))

        # Run tcpm to install itself:
        bootstrap_tcpm_cmd = [
            sys.executable,
            baseline_path,
            "--packages-dir", os.path.join(
                repo_base,
                "tc_packages", "cache"),
            temp_tcpm_bin,
            "-v", "--package-dir", module_path,
            "install", "--baseline-bootstrapped",
            tcpm_base
            ]
        print("tools/baseline-tcpm.py: info: " +
            "running tcpm bootstrap cmd: " +
            str(bootstrap_tcpm_cmd))
        result = subprocess.call(bootstrap_tcpm_cmd,
            cwd=repo_base)
        if result != 0:
            print("tools/baseline-tcpm.py: error: " +
                "tcpm failed to install itself.",
                file=sys.stderr)
            sys.exit(1)
        assert(not os.path.exists(
            os.path.join(repo_base, "tc_packages",
            "io.tethercode.stdlib", "tc_packages")))

        # Ensure binary was created:
        if not os.path.exists(local_tcpm_path):
            print("tools/baseline-tcpm.py: error: " +
                "tcpm bootstrap reported success, " +
                "but no tcpm binary present.",
                file=sys.stderr)
            sys.exit(1)
    assert(os.path.exists(local_tcpm_path))

    symlink_ok = True
    if platform.system() != "Windows" and \
            (not os.path.exists("/usr/bin/tcpm") or \
            not os.path.islink("/usr/bin/tcpm") or \
            os.path.normpath(os.readlink("/usr/bin/tcpm")) !=
            os.path.normpath(os.path.abspath(local_tcpm_path))):
        symlink_ok = False

    # If not root, relaunch ourselves with sudo:
    if platform.system() != "Windows" and \
            os.geteuid() != 0 and not symlink_ok:
        print("tools/baseline-tcpm.py: warning: root " +
            "privilegues required for /usr/bin/tcpm " +
            "install, rerunning self with su...",
            file=sys.stderr)
        cmd = "su", "root", "-c", sys.executable + " '" + __file__ + "'"
        exit_code = subprocess.call(cmd, shell=False)
        sys.exit(exit_code)

    # Add system-wide tcpm command:
    if platform.system() != "Windows" and not symlink_ok:
        # Add symlink:
        print("tools/baseline-tcpm.py: info: adding " +
            "global /usr/bin/tcpm -> " + os.path.abspath(
            local_tcpm_path) + " symlink...")

        if os.path.exists("/usr/bin/tcpm"):
            os.remove("/usr/bin/tcpm")
        os.symlink(os.path.normpath(
            os.path.abspath(local_tcpm_path)),
            "/usr/bin/tcpm")
        return "tcpm"
    elif platform.system() == "Windows":
        raise RuntimeError("code path not implemented")
        
tcpm_cmd = get_tcpm()

