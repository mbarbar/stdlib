
# tethercode stdlib - random.tc
# Copyright (c) 2016-2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

preccheck_if translator_name == "baseline"
import pythonlang
pythonlang.do_import("random", "_python_random")
pythonlang.do_import("uuid", "_python_uuid")
preccheck_end

class PseudoRandomGenerator {
    let seed
    preccheck_if translator_name == "baseline"
    let python_random_state
    preccheck_end
    func init(seed) {
        self.seed = seed
        preccheck_if translator_name == "baseline"
        _python_random.seed(seed)
        self.python_random_state = _python_random.getstate()
        preccheck_else
        throw RuntimeException("code path not implemented")
        preccheck_end
    }
    func randint(min_value, max_value) {
        if max_value <= min_value {
            return min_value
        }
        preccheck_if translator_name == "baseline"
        _python_random.setstate(self.python_random_state)
        let result = _python_random.randint(min_value, max_value)
        self.python_random_state = _python_random.getstate()
        preccheck_else
        let result
        throw RuntimeException("code path not implemented")
        preccheck_end
        return result
    }
    func randbytes(len) {
        throw RuntimeException("code path not implemented")
    }
}

class SecureRandomGenerator {
    preccheck_if translator_name == "baseline"
    let python_secure_random
    preccheck_end
    func init {
        preccheck_if translator_name == "baseline"
        self.python_secure_randome = _python_random.SystemRandom()
        preccheck_else
        throw RuntimeException("code path not implemented")
        preccheck_end
    }
    func randint(min_value, max_value) {
        if max_value <= min_value {
            return min_value
        }
        let result
        preccheck_if translator_name == "baseline"
        result = self.python_secure_random.randint(min_value, max_value)
        preccheck_else
        throw RuntimeException("code path not implemented")
        preccheck_end
        return result
    }
    func randbytes(len) {
    preccheck_if translator_name == "baseline"
        return _python_random.urandom(len)
    preccheck_else
        throw RuntimeException("code path not implemented")
    preccheck_end
    }
}

func pseudogen(seed) {
preccheck_if translator_name == "baseline"
    return PseudoRandomGenerator()
preccheck_else
    throw RuntimeException("code path not implemented")
preccheck_end
}

func securegen() {
preccheck_if translator_name == "baseline"
    return SecureRandomGenerator()
preccheck_else
    throw RuntimeException("code path not implemented")
preccheck_end
}

preccheck_if translator_name == "baseline"
let system_random_gen = null
preccheck_end
func randint(min_value, max_value) {  # FIXME: thread safety
preccheck_if translator_name == "baseline"
    if system_random_gen == null {
        system_random_gen = _python_random.SystemRandom()
    }
    return system_random_gen.randint(min_value, max_value)
preccheck_else
    throw RuntimeException("code path not implemented")
preccheck_end
}

func randbytes(len) {
preccheck_if translator_name == "baseline"
    if system_random_gen == null {   # FIXME: thread safety
        system_random_gen = _python_random.SystemRandom()
    }
    return system_random_gen.randbytes(len)
preccheck_else
    throw RuntimeException("code path not implemented")
preccheck_end
}

func uuid4_binary {
preccheck_if translator_name == "baseline"
    return _python_uuid.uuid4().bytes
preccheck_else
    throw RuntimeException("code path not implemented")
preccheck_end
}

func uuid4 {
preccheck_if translator_name == "baseline"
    return tostring(_python_uuid.uuid4())
preccheck_else
    throw RuntimeException("code path not implemented")
preccheck_end
}


