
# Installation

**Important**: if you just want to have a working version of the tethercode
SDK, we recommend you go to the [download page of the pre-compiled SDK](
https://tethercode.io/go/get ). This guide is for developers who want to
build and use an experimental development version of tethercode.

## Build

### Prerequisites

Building and using tethercode from scratch requires the following tools to
be available on your system:

* Python 3 (3.3 or newer)
* gcc C compiler
* autotools / autoconf
* GNU make
* A Linux system where you need to run the build on (Windows / Mac are
  currently not supported for building the base tools themselves)
* *(Highly recommended)* docker - https://docker.io
* *(Highly recommended)* docker-compose

Docker isn't strictly required, but it will allow you to build the bytecode
VM with maximal target platform support, so that your resulting compiled
Wobbly SDK can build for as much target platforms right out of the box as
possible.

If you also want to build the HTML documentation files, you will need those
tools in addition:

* pandoc

### Building tethercode

Building tethercc and/or the documentation is provided through GNU make, and
the `Makefile` in the repository root. Open up a terminal and issue one of the
following commands:

* Command `make`:
  This builds tethercc, the tethercode stdlib, tcpm and the bytecode VM.

* Command `make docs`:
  This command generates the HTML documentation inside the docs/ folder.

* Command `make check`:
  This command runs various unit tests for the compiler and baseline runner.

If you encounter problems, please report them to the discussion list:
https://lists.wobblylang.org/mailman3/lists/discuss.lists.wobblylang.org/
or [report a bug](CONTRIBUTING.md).

