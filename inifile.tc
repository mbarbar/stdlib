
# tethercode stdlib - inifile.tc
# Copyright (c) 2016-2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import io
import path

func _parse_lines(stream) {
    ## This helper function splits up the contents of a stream into separate
    ## lines and returns the result
    ## The stream must be utf-8, binary streams are not supported.

    let prev_was_carriage_return = false
    let lines = []
    let eof = false
    while not eof {
        let line = ""
        while true {
            let c = stream.read(1)
            # Handle line breaks:
            if c == "" or c == "\n" or c == "\r" {
                if c == "" {
                    eof = true
                }
                if c == "\r" {
                    prev_was_carriage_return = true
                } elseif c == "\n" or c == "" {
                    if prev_was_carriage_return and
                            line.length == 0 {
                        # Skip this line.
                        prev_was_carriage_return = false
                        break
                    }
                    prev_was_carriage_return = false
                }
                lines.add(line)
                line = ""
                break
            } else {
                prev_was_carriage_return = false
                line += c
            }
        }
    }
    return lines
}

func dump(config_data) {
    
}

func parse_file(stream) {
    # If supplied with string/bytes, interpret as filename:
    let close_stream_at_end = false
    if type(stream) == BaseType.string or
            type(stream) == BaseType.bytes {
        stream = io.open(stream, "r")
        close_stream_at_end = true
    }
    
    let lines = _parse_lines(stream)
    let result = {}
    let current_section = null

    # Parse lines:
    for line in lines {
        # Skip comments:
        if line.startswith("#") or line.startswith(";") {
            continue
        }

        # Parse contents:
        if line.startswith("[") and line.rtrim().endswith("]") {
            # Section header:
            line = line.rtrim()
            current_section = line[1:]
            current_section = current_section[:current_section.length - 1]
            if current_section.length > 0 {
                if not (current_section in result) {
                    result[current_section] = {}
                }
            } else {
                current_section = null
            }
            continue
        } elseif not line.startswith("[") {
            # Value assignment:
            let assign_index = line.find("=")
            if assign_index > 0 {
                let assign_val = line[:assign_index]
                assign_val = assign_val.trim()
                let assigned_val = line[assign_index+1:]
                assigned_val = assigned_val.ltrim()
                if assign_val.length > 0 {
                    result[current_section][assign_val] = assigned_val
                }
            }
        } else {
            if close_stream_at_end {
                stream.close()
            }
            throw ValueException("invalid syntax in config file")
        }
    }
    if close_stream_at_end {
        stream.close()
    }
    return result
}

func parse(str) {
    let stream = io.stream_from_string(str)
    return parse_file(stream)
}

