
# Source Code License

All contained source code files are, unless specified otherwise, under the
`zlib license` which is as follows:

```
Copyright (c) 2016-2017,
tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgement in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
```

# Artwork / Misc License

## Non-logo artwork and documentation

All artwork (images, ..) and all documentation files inside the docs/ folder
in the repository, with the exception of the tethercode logo itself (all files
inside misc/logo/), are under the license `CC-By-4.0`
https://creativecommons.org/licenses/by/4.0/ - please attribute to
"tethercode dev team - https://tethercode.io/go/authors ".

**Important note for binary releases:**

Please note if you are finding this license as part of a release archive and
not the source repository, so that no `misc/` folder is present, the
tethercode logo is probably named `logo.png` or `logo.svg` and/or compiled
into the executable files of the program(s).
**The logo artwork license terms below still apply.**

## Logo artwork

All the tethercode logo files originally found in misc/logo/ (just called
"tethercode logo" or "logo" in the following) are owned by Jonas Thiem,
Copyright (C) 2016-2017, All Rights Reserved.
The tethercode logo may only be used under the following terms (which may be
subject to change at any point, check https://tethercode.io/go/license for
the latest version):

You may redistribute the tethercode logo as part of unmodified copies of
the official tethercode software source archive (as obtainable through
https://tethercode.io/go/source-latest , or any revision as found on the git
master branch of the git repository at https://gitlab.com/tethercode/stdlib ).
As soon as you add, change or omit any of the files when redistributing the
official tethercode software source archive, you may no longer include
the logo.

You may also distribute the tethercode logo as part of unmodified copies
of a release archive created by running `tcpm publish` inside the official
tethercode software source archive using an official unmodified version of
tcpm as found at https://gitlab.com/tethercode/tcpm .

Exceptions to these terms can only be obtained in written form.
Write to: `jonas ['at'] thiem ['dot'] email` for inquiries.

