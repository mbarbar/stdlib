
# tethercode stdlib - sys.tc
# Copyright (c) 2016-2017,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import encoding

preccheck_if translator_name == "baseline"
import pythonlang
pythonlang.do_import("sys", "_python_sys")
pythonlang.do_import("os", "_python_os")
pythonlang.do_import("subprocess", "_python_subprocess")
preccheck_else
preccheck_end

let argv = []
let stderr = null

func on_import {
    preccheck_if translator_name == "baseline"
    argv = _python_sys.argv[1:]
    stderr = _python_sys.stderr
    preccheck_end
}

func exit(value) {
    preccheck_if translator_name == "baseline"
    _python_sys.exit(value)
    preccheck_end
}

func _cmd_split_helper(cmd) {
    let is_bytes = false
    let single_quote = "'"
    let double_quote = '"'
    let space = " "
    let empty = ""
    let quoted = null
    if type(cmd) == BaseType.bytes {
        is_bytes = true
        single_quote = b"'"
        double_quote = b'"'
        space = b" "
        empty = b""
    }

    let result = []
    let current_item = empty
    let i = 0
    while i < cmd.length {
        if quoted != null {
            if cmd[i] == quoted {
                quoted = null
                i += 1
                continue
            }
        } else {
            if cmd[i] == single_quote or
                    cmd[i] == double_quote {
                quoted = cmd[i]
                i += 1
                continue
            }
            if cmd[i] == space {
                if current_item.length > 0 {
                    result.add(current_item)
                    current_item = empty
                }
                i += 1
                continue
            }
        }
        preccheck_if translator_name == "baseline"
        current_item += cmd[i:i+1]
        preccheck_else
        current_item += cmd[i]
        preccheck_end
        i += 1
    }
    if current_item.length > 0 {
        result.add(current_item)
    }
    return result
}

func run(cmd, cwd=null, hide_stdout=false, hide_stderr=false) {
    if type(cmd) == BaseType.list {
        if type(cmd[0]) == BaseType.bytes {
            let t = b""
            let first = true
            for part in cmd {
                if first {
                    first = false
                } else {
                    t += b" "
                }
                t += b"'" + part.replace(b"'", b"'\"'\"'") + b"'"
            }
            cmd = t
        } else {
            let t = ""
            let first = true
            for part in cmd {
                if not type(part) == BaseType.string {
                    throw TypeException("mixed unicode and bytes " +
                        "argument types are not allowed for sys.run")
                }
                if first {
                    first = false
                } else {
                    t += " "
                }
                t += "'" + part.replace("'", "'\"'\"'") + "'"
            }
            cmd = encoding.encode(t)
        }
    } else {
        # Split up the command first:
        throw RuntimeException("code path not implemented. Type: " +
            tostring(type(cmd)))
    }
    preccheck_if translator_name == "baseline"
    if cwd == null {
        cwd = _python_os.fsencode(_python_os.getcwd())
    }
    let stdout_value = null
    if hide_stdout {
        stdout_value = _python_subprocess.DEVNULL
    }
    let stderr_value = null
    if hide_stderr {
        stderr_value = _python_subprocess.DEVNULL
    }
    return _python_subprocess.call(
        _cmd_split_helper(cmd),
        cwd=cwd, stdout=stdout_value, stderr=stderr_value)
    preccheck_else
    throw RuntimeException("code path not implemented")
    preccheck_end
}

func pid {
    preccheck_if translator_name == "baseline"
    return _python_os.getpid()
    preccheck_else
    throw RuntimeException("code path not implemented")
    preccheck_end
}

func getenv(v) {
    let return_bytes = false
    if type(v) == BaseType.bytes {
        return_bytes = true
    }
    preccheck_if translator_name == "baseline"
    if type(v) == BaseType.bytes {
        v = encoding.decode(v)
    }
    let result = _python_os.getenv(v)
    if return_bytes {
        result = encoding.encode(result)
    }
    return result
    preccheck_else
    throw RuntimeException("code path not implemented")
    preccheck_end
}

