% tethercode docs > Limits

<!--
    This file is part of the tethercode documentation.
    Licensed under
    CC-By-4.0 https://creativecommons.org/licenses/by/4.0/
    Attribution:
    tethercode dev team https://tethercode.io/go/authors
-->

[Return to the documentation index](index.md)

# Limits

This page holds info on various practical limits which tethercode
programs are subject to (e.g. range of the number type).

## Range of the number type

Numbers in tethercode will internally get created as an integer type if you
assign them as a literal with no decimal part.

In this integer representation, the number will be presented as a 64bit
signed integer as long as it fits into that range and it will get
transparently converted to a growing bigint type if it exceeds this range.
Effectively, this means numbers without decimal digits can grow as large as
your computer's memory allows, but they will be much slower once they exceed
the 64bit signed integer range.

When any operator is applied with one of the involved operands being a floating
point number (e.g. any number created from the start from a numeric literal
with decimal digits) or for some divisions, the result will be a floating point
number internally. For this, the runtime uses a 64bit double with the usual
restrictions (e.g. 32bit signed int range can be fully represented, above you
may see more and more precision loss).

## Recursion depth / stack

The stack of the tethercode runtime is allocated on the heap and it can grow
dynamically. This means you can recurse as deep as you want until your
computer's memory is full (the stack can grow up to megabytes or even
gigabytes of size).

Currently, tethercode doesn't do any tail recursion or other recursion
optimization, so beware that if you recurse forever your memory WILL be
exhausted eventually. It will just take a lot longer than in other
languages which are limited by the OS stack size.

## Function call parameters

There is no limit for the amount of function call parameters or keyword
arguments. However, having a really large amount of arguments will slow
down function calls.

## Object size, entries in a list, dictionary or set

There is no limit for object sizes other than your computer's memory and
what your standard C library's allocator will allow as maximum allocated
size. The same applies to entries in a list or dictionary or a set, although
the hash table performance may degrade if you get into the millions of
entries range.

## Length of literals, identifiers, ..

There is no limit for the length of literals, identifiers or any other token.

## Amount of objects

There is no limit for the amount of objects other than your computer's
memory. However, tethercode is garbage-collected which means after you
stop using & referencing any object, it may stay around for a longer time
until finally being removed. This means if you create huge amounts of data
that just about fit into memory and unreference it over and over, you may
actually exceed your computer's memory even though the amount of objects
you use in your program would in theory fit into it. This is a limitation
of how the runtime works that cannot be easily avoided.





