% tethercode docs > Principles

<!--
    This file is part of the tethercode documentation.
    Licensed under
    CC-By-4.0 https://creativecommons.org/licenses/by/4.0/
    Attribution:
    tethercode dev team https://tethercode.io/go/authors
-->

[Return to the documentation index](index.md)

# Principles

This page lists a few underlying principles that were adhered to during
the design of the tethercode language and concurrency runtime behavior.

Please note that the tethercode language was also designed with quite a
bit of pragmatism and not by a decades of experience language designer,
so read this with a grain of salt and feel free to [point out issues or
suggest improvements](https://gitlab.com/tethercode/stdlib/issues)!

## Overall

- *Easy enough for beginners, useful enough for experts:*

  The tethercode language shall be designed such that all integrated
  mechanisms are relatively easy to understand for a beginner, while
  still incorporating all mechanisms needed for experts to write code
  productively.

  Based on this principle, the syntax is relatively clean with:

  - no type annotations

  - operators written out as a word unless well-known arithmetic operators

  - a duplicate method to specify methods (simple `func abc() {return d}`
    vs lambda `() -> d`)

    - with the idea being to have both an easily readable stanard variant,
      while providing inline lambdas for experts where really needed

  - lambdas intentionally limited to one expression to avoid unnecessarily
    cluttered inline code which could be hard to read

  - no complex special syntax like declarators or list comprehensions as
    seen in Python, or generics/templating as seen in Java or C++

- *Complex enough to be useful, simple enough not to be confusing*

  The tethercode language shall be designed in such a way that reasonable
  built-in types for various tasks exist, but that there aren't unnecessary
  many mechanisms to achieve the same tasks. New features shall always
  be weighted for usefulness against additional clutter.

  This is the reason for many intentional omissions like:

  - `for` loops with counters rather than for each as in C/C++
  - no operator overloading other than `==`/`equals` (because most tasks can
    be achieved without this already)
  - no `switch` statement in favor of existing `if`/`elseif`
  - no coroutines or promises in favor of simple sequential code style and
    moving the entire algorithm into a separate thread if I/O wait must be
    avoided
  - no access to pointers or direct memory modification, and many OS features
    like sockets are wrapped into easier but sometimes also more limited
    abstractions that are more intuitive to use

- *Compile-time checks should assist, not get in the way*

  Impact on treatment of warnings and errors:

  The tethercode compiler strives to provide warnings where reasonable,
  but won't error on e.g. unavoidable type errors since the user intention may
  be to throw and catch them intentionally. Any code that is possibly nonsense
  but valid in the language semantics shall not lead to a compiler error but
  to a warning instead.

  Impact on overall language design:

  The language design will remain as close to a fully dynamic language as
  possible (with the exception of the static scoping) and no language features
  shall be introduced purely to accomodate better compile time checks, unless
  the benefits are considered huge and they don't add much clutter.

- *Nobody should have to care about dependencies for simple applications*

  Impact on licensing:

  The tethercode standard lib is designed to not incorporate or depend on
  anything of which the license has special conditions like requiring
  attribution at runtime. This means almost all licenses except `CC0`,
  `zlib` and very few others are unacceptable (including `MIT`, `BSD`, ..).

  Impact on compilation backend:

  The tethercode compiler is designed to compile to C with no dependencies,
  and to include resources into the binary where possible. This leads to
  single binary programs that run independently, and are trivial to deploy
  while keeping the speed benefits of C.

## Concurrency

Tethercode's concurrency has a few basic principles.

The basic standpoint of tethercode is to support true but simple concurrency
wherever possible, but maintaining a stable interpreter state and data
consistency always takes precedence.

*An important preface on the concurrency design of tethercode:*

Please note while there is a heavy emphasis on interpreter safety, this
won't prevent any logical race conditions in your code. tethercode has
no advanced concurrency constructs to hold your hand here and you are
encouraged to use libraries for such functionality. This allows you the
freedom to pick whichever paradigms you see fit.

- *Simple shared data:*

  It is recognized by the creators of tethercode that message passing is
  in many cases superior. However, for the sake of simple universal use of
  threads, this shall never be enforced. Again, you are encouraged to pick
  the libraries & paradigms at your own discretion. Therefore, all variables
  shall always accessible from all threads without any special mechanisms
  (like `volatile` annotations or required locking).

- *Strive to support true concurrency:*

  When possible and reasonable, true hardware threads are preferred over
  single-thread coroutines. This might also be pursued when it hurts
  single-thread performance for the average single-threaded program that
  doesn't make any use of this. However, crash safety takes precedence
  over this rule (see below).

- *Always maintain data consistency:*

  When something is altered in a single assignment statement, another
  thread that does concurrent access without explicit locking shall
  always consistently see the old value or the new value.

- *Don't crash unless it can't be helped:*

  Unless an external C library corrupts memory due to threading issues,
  the byte code interpreter shall always try to maintain a consistent state
  and raise proper errors if the concurrency confuses your code - and the
  standard library will generally be thread-safe unless noted otherwise in
  the manual.

This means threading is simple to use (with the "async" keyword) and
so is sharing data between threads, but this results in some performance
trade-offs.

