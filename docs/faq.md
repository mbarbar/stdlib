% tethercode docs > FAQ

<!--
    This file is part of the tethercode documentation.
    Licensed under
    CC-By-4.0 https://creativecommons.org/licenses/by/4.0/
    Attribution:
    tethercode dev team https://tethercode.io/go/authors
-->

[Return to the documentation index](index.md)

# Frequently Asked Questions (FAQ)

This document contains questions that are either asked often, or which
we anticipate willl be asked often about tethercode.

## `__tccache__` and `tc_packages`

**Q: There are some weird `__tccache__` and `tc_packages` folders
appearing, can I delete those?**

A: The `__tccache__` folders contain cached intermediate results from previous
compilations of your program. You can safely delete them but it will result
in the next compilation to be slower since everything will need to be
recompiled from scratch.

The `tc_packages` folder is created by `tcpm` (tethercode package manager)
when you install the dependencies of your project. If you delete it, you
will need to install the dependencies again or you'll most likely get
errors for the affected module imports when compiling your project the
next time.





