% tethercode docs > Runtime

<!--
    This file is part of the tethercode documentation.
    Licensed under
    CC-By-4.0 https://creativecommons.org/licenses/by/4.0/
    Attribution:
    tethercode dev team https://tethercode.io/go/authors
-->

[Return to the documentation index](index.md)

# Runtime

This page describes the technical properties of the tethercode runtime.

A tethercode program is compiled by tethercc creating a bytecode image
containing tetherasm bytecode in binary form along with all resoruces
compiled into the program, and then the bytecode image being appended to
a tethervm binary and the result being written to disk. The tethervm is
written in C and interprets the binary bytecode when started to produce
the actual program behavior.

## tethervm

The tethervm is written in C and interprets binary bytecode. It is included
in any tethercode program binary. This also means if there are any security
updates to tethervm, you need to get a new tethercode SDK with a new tethercc
that uses the fixed version of tethervm and recompile all your programs.

## tetherasm

The tethercode language uses a custom bytecode language named 'tetherasm'.
If you want to read the bytecode there is no need to dig around in the binary
file, instead tell tethercc to omit human-readable text bytecode instead of
an executable binary with the `--intermediate-output tetherasm` option.

There is currently no in-depth documentation of tetherasm, although a lot
of it should be fairly self-explanatory.

You can also compile such a 'tetherasm' file directly to an executable if you
want with the help of the according helper program with that name - however,
beware that you can create invalid bytecode sequences quite easily which will
crash at runtime, e.g. by not resizing the stack before accessing a certain
slot on the stack outside of its current boundaries. This is intentional,
because the compiler is designed not to do this.

## Limits

To read up on effective limits of the runtime, [go to the limits section](
limits.md). In general, the runtime trades versatility and scalability for
performance, so there aren't that many other than your computer's memory.
The notable exception is currently the numerical range possible for floating
point numbers (numbers with digits after the decimal point).




