% tethercode docs > Formal Specification

<!--
    This file is part of the tethercode documentation.
    Licensed under
    CC-By-4.0 https://creativecommons.org/licenses/by/4.0/
    Attribution:
    tethercode dev team https://tethercode.io/go/authors
-->

[Return to the documentation index](index.md)

# Formal Specification

This section describes the basic grammar and syntax of tethercode,
as well as a specification of the semantics.
It contains a formal definition of the grammar as well.

## Grammar

The following is a formal specification of the production rules that
make up valid tethercode grammar. This grammar is for documentation
purposes and not the exact same set of rules used by the parser.
Therefore, if you discover any inconsistencies compared to actual
compiler behavior, please report it so it can be fixed.

Grammar tokens and helpers (specified as regular expressions):
```
<identifier> = /[a-zA-Z_][a-zA-Z0-9_]*/
<number literal> = /([0-9]+(\.[0-9][0-9]*)?)|0x[0-9a-fA-F]+/
<boolean literal> = /(true|false)/
<string literal> = /"([^"]|\\")*"|'([^']|\\')*'/
<bytes literal> = /b"([^"]|\\")*"|b'([^']|\\')*'/
<whitespace> = /(\n|\r|\t| )*/
```

Grammar production rules:

```
IMPORTANT: this grammar is a superset of valid code, and the
following additional rules need to be followed:

- Operator precedence is not baked into these rules for the sake of
  readability and simplicity.
  Instead, you are expected to choose the correct rule by order of
  most loosely binding operator first when there are multiple binary
  or unary operator rules applicable (see section on operator
  precedence below).

- The lexical scope needs to be valid. See section on scoping on how
  to check this, but basically variables need to be defined before
  they can be used in an identifier referencing them.

- Top level let statements may not contain calls or object creations
  (which are done using the call expression on class types) in their
  assigned values if a default assigned value is specified.

<starting symbol> ::= <top level statement list>

<top level statement list> ::= <top level statement>
    | <top level statement> <top level statement list>

<top level statement> ::= <let statement>
    | <named function definition statement>
    | <class definition statement>

<code block> ::= <statement>*

<statement> ::= <let statement>
    | <if conditional statement>
    | <while conditional statement>
    | <for loop statement>
    | <assignment statement>
    | <arithmetic assignment statement>
    | <named function definition statement>
    | <call statement>
    | <return statement>
    | <continue statement>
    | <break statement>

<let statement> ::= 'let' <identifier>
    | 'let' <identifier> '=' <expression>

<if conditional statement> ::=
    'if' <expression> '{' <code block> '}' <elseif block>* <else block>?

<elseif block> ::= 'elseif' <expression> '{' <code block> '}'

<else block> ::= 'else' '{' <code block> '}'

<while conditional statement> ::=
    'while' <expression> '{' <code block '}'

<for loop statement> ::=
    'for' <identifier> 'in' <expression> '{' <code block> '}'

<assignment statement> ::=
    <lvalue> '=' <expression>

<lvalue> ::= <identifier>
    | <lvalue> '.' <identifier>
    | <lvalue> '[' <expression> ']'
    | <lvalue> '(' <expression> ')'

<arithmetic assignment statement> ::= <lvalue> '+=' <expression>
    | <lvalue> '-=' <expression>
    | <lvalue> '/=' <expression>
    | <lvalue> '*=' <expression>
    | <lvalue> '|=' <expression>
    | <lvalue> '%=' <expression>
    | <lvalue> '&=' <expression>
    | <lvalue> '~=' <expression>

<named function definition statement> ::=
    'func' <identifier> <function definition
        argument list>? '{' <code block> '}'

<function definition argument list> ::=
    '(' (<function definition argument> ',')*
        <function definition argument> ')'
    | '(' <function definition argument?> ')'

<function definition argument> ::= <identifier>
    | 'multiarg' <identifier>
    | <identifier> '=' <expression>

<class definition statement> ::=
    'class' <identifier> <class extends info>? '{'
        <class contents> '}'

<class extends info> ::= 'extends' <expression>

<class contents> ::= <class content item>
    | <class content item> <class contents>

<class content item> ::= <named function definition statement>
    | <let statement>
    | <class definition statement>

<expression> ::= <basic literal>
    | <lambda function definition>
    | <unary operator>
    | <member access binary operator>
    | <indexing binary operator>
    | <slicing binary operator>
    | <regular binary operator>
    | <call>
    | '(' expression ')'
    | <list constructor>
    | <dictionary constructor>
    | <set constructor>
    | 'self'
    | 'super'

<list constructor> ::= '[' <expression>? ']'
    | '[' (<expression> ',')* <expression> ']'

<dictionary constructor> ::= '{' <dictionary assignment>? '}'
    | '{' (<dictionary assigment> ',')* <dictionary assignment> '}'

<dictionary assignment> ::= <expression> ':' <expression>

<set constructor> ::= '{<' <expression>? '>}'
    | '{<' (<expression> ',')* <expression> '>}'

<call statement> ::= <call>
    | 'async' <call>

<call> ::=
    <expression> <function call argument list>

<function call argument list> ::=
    '(' (<function call argument> ',')*
        <function call argument> ')'
    | '(' <function call argument?> ')'

<function call argument> ::= <expression>
    | 'multiarg' <expression>
    | <identifier> '=' <expression>

<regular binary operator> ::= <expression> '+' <expression>
    | <expression> '-' <expression>
    | <expression> '/' <expression>
    | <expression> '*' <expression>
    | <expression> 'and' <expression>
    | <expression> 'or' <expression>
    | <expression> 'in' <expression>
    | <expression> '%' <expression>
    | <expression> '&' <expression>
    | <expression> '|' <expression>

<lambda function definition> ::=
    <function definition argument> '->' <expression>
    | <function definition argument list> '->' <expression>

<member access binary operator> ::= <expression> '.' <identifier>

<indexing binary operator> ::= <expression> '[' <expression> ']'

<slicing binary operator> ::= <expression> '[' <expression> ':' <expression> ']'
    | <expression> '[' ':' <expression> ']'
    | <expression> '[' <expression> ':' ']'

<unary operator> ::= 'not' <expression>
    | '+' <expression>
    | '-' <expression>
    | '~' <expression>

<basic literal> ::= <string literal>
    | <number literal>
    | <boolean literal>
    | <bytes literal>
    | <identifier>
    | 'null'

<break statement> ::= 'break'

<continue statement> ::= 'continue'

<return statement> ::= 'return' <expression>

```

## Basic program structure / `main` procedure

A tethercode program cannot have non-trivial code outside of function
definitions and class definitions (only simple code which doesn't contain
function calls or object instantiations may be contained in top-level
let statements).

As an entry point, a program must contain a function named `main` in the file
which was given as an argument to the compiler as a starting point for
compilation (and from where imports of all other files/modules will be
processed as necessary). Not providing a top-level main function in the
starting code file passed to the compiler will lead to a compilation error.

The main function is like all other functions and has no restrictions in
what it can do, it's only special purpose is that it's called at program
start.

Furthermore, if any code file processed while compiling the program
contains an `on_import` function, all of those functions will be run in
an arbitrary order before the `main` function runs at program start.
Those functions are meant for additional module-specific
initialization and should never be written such that they run for a
long time or possibly block on resource access for a long time.

The code files imported may also contain `main` functions themselves which
won't be treated in any special way (and not run on program start) - this
is so that module code files can both be compiled as a standalone tool
(in which case their `main` will be run) or imported and used as a library
(in which case it won't). 


## Data types

The values in tethercode can be of various data types.
tethercode differentiates between basic data types (which are built-in), and
class-based types.
`items` or `values` refers to values of all possible types,
`object` refers to any values of a class-based type.

The builtin `type()` function which takes a single value as a parameter
allows you to retrieve the type of any given value at runtime if needed.

### Basic data types

The basic data types are:

```
null        - A 'null' type for specifically non-specified value
number      - A decimal or integer number
string      - A unicode string
boolean     - A boolean value (true or false)
bytes       - A sequence of raw bytes
function    - A callable function
list        - A list of values
dictionary  - A key value storage of values
set         - An unordered set of values
```
All basic data types except for `list`, `dictionary` and `set` are
always passed by value and non-mutable (any operation or function call
on them will always return a new instance, never modify the given one).
The composite types `list`, `dictionary` and `set` are passed by reference,
so that any sort of modifying function will usually change the given instance
itself. If you want to have an independent new copy of an instance of such
a data type, use the `.copy()` member.

### Class-based data types

The class-based data types are based on the idea of user-defined types
which behave according to a user-specified object class.

A `class`, created by the class statement (see grammar specification above)
describes how an object instantiation of a such specific `class` behaves:

##### Class object behavior

An object instantiation has members which can be accessed by the member
access operator. The members available are exactly those specified on the
`class` definition:

A member may be either specified with the `<let statement>` in which case
it may contain any data type (including functions) at runtime, and in
which case it can be changed at runtime, or a `<named function definition>`
in which case it will be unchangeable at runtime and always refer to the
given function.

An object member can be referenced on an object by using
`<object_identifier>` '.' `<member identifier>`. Such a reference may
emit a `TypeException` at runtime if this member doesn't actually exist
on the underlying `class`, or a compile warning at compile time if the
compiler is certain that it can never be present at runtime (this is rare
and it shouldn't be expected to be checked reliably).

Each object instantiation has its own copy of the members, and if a member
is changed with an assignment to the referenced member at runtime, the
value will be changed only for the given object instantiation. However,
all object instantiation start out with the default value assigned in
the respective `<let statement>` of the underlying `<class definition>`.

A class definition can derive from another class with `extends` keyword.
In such a case, all members not redefined by the current class will be
inherited by the class that it derives from (and all the classes that
parent class derives from etc).

##### Class instantiation

The user can instantiate as many objects as they like from such
a `class` type at runtime, by using the `<call>` expression on an
identifier referencing a class.

##### Class members are static

The `class` itself with all the contained members is specified statically
at compile time, and the members a class or an object instantiation contains
cannot be altered at runtime.

#### `object` base class

All `class` types are internally derived from the built-in `object` base
class.

#### type() and `metaclass` meta type

Using `type()` on any such object instance will return the user-defined
`class` type it was created from.
Such a `class` type itself is of the built-in `metaclass` type.
The `metaclass` type is also derived of the `object` type.

#### Classes are passed along by reference

All class instantiated objects (also called "instance
object" or just "object"), all class types (also called "classes") and
the meta class are always passed by reference.

Use the built-in `.copy()` member to create a new copied instance of an
object. A class may contain a method called "copy"

However, only an object is mutable, the classes and the metaclass are
created at compile time and cannot be modified in any way at runtime.

## Operators

### Math operators

tethercode supports the following math operators:

- **Multiplication:** ```*```
- **Addition**: ```+```
- **Division**: ```/```
- **Substraction**: ```-```

Math operators can only be applied to two instances of the *number*
data type, with the exception of the *addition operator* ```+```
which can be used for concatenation of two *string* variables,
concatenation of two *list* variables and for combining two *set*
variables.

Please note further operations on numbers are available through the
standard library and other functions, these operators here are just the
functionality directly available through the basic language syntax.

### Boolean operators

The following are the boolean operators which can be applied to the
*boolean* data type and the *null* datatype (which is interpreted as
*false*):

- ``and``  (applied to two elements)
- ``or``   (applied to two elements)
- ``not``  (applied to a single element)

Usage examples:
```tethercode
let i = 5
let is_it_five = (i == 5)
let is_it_not_five = not is_it_five
let is_one_or_two = (i == 1) or (i == 2)
```

### 'in' operator

The ```in``` operator allows to check if any item is contained in a list,
set or dictionary (as a key). It evaluates to a *boolean* expression with
the value being *false* if the item is not contained, and *true* if it is
contained.

Usage example:
```tethercode
print("Is 2 in the list of numbers 1, 2, 3? -> " +
    (2 in [1, 2, 3]))
```

### '.' (member access) operator

The member access operator ```.``` operator is a binary operator which will
retrieve the member of the object represented by what is on the left-hand
side, with the name of the identifier token on the right-hand side.

Usage example:
```tethercode
class Vehicle {
    let wheels = 4
}
my_car = Vehicle()
print("The amount of wheels are: " + my_car.wheels)
```

### [..] (indexing) operator

The indexing operator ```[...]``` is a binary operator where the
object represented by the expression left to the opening bracket `[`
will be indexed with what is represented by the expression inside the
`[`, `]` brackets.

The expression inside the brackets used for indexing can be one of the
following three expression types:

#### Indexing with an index expression

An indexing expression is simply a number `i` (or some function call or
computation that results in a number), and an indexing expression can be
used with the indexing operator on the basic `list`, `string` and `bytes`
datatypes, and it will retrieve the i-th list item, the i-th unicode
character, and the i-th byte respectively. (the first item is item `0`,
the indexing is 0-based) 

However, if the index is out of bounds, it will throw a `IndexException`
instead.

Usage example:
```tethercode
# Create a list to use for indexing examples:
let l = [12, 17, "hello"]

# Print out various items of the list, picked out with the indexing operator:
print("First item: " + l[0])
print("Second item: " + l[1])
print("Third item: " + l[2])

# This would result in an IndexException: print("Fourth item: " + l[3])
```

#### Indexing with a key expression

A key expression is an expression that represents an object which is looked
up, and can be used on the basic `dictionary` type to retrieve the
corresponding stored value in the associative storage of the dictionary.

However, if the key is not found, the operator will throw a `KeyException`.
(if you want to check if a key is contained without triggering and catching
a `KeyException`, use the `in` operator)

Usage example:
```tethercode
# Create a dictionary for a key access example:
let employee_age = {
    "John Doe" : 25,
    "Jane Doe" : 27,
}

# Access the data with the indexing operator:
print("Employee age John Doe: " + employee_age["John Doe"])

# This would result in a KeyException: print(employee_age["Jack Thompson"])
```

#### Indexing with a slicing expression

A slicing expression is made up of two indexing expressions `start`, `end`
notated with a colon character in between: `start:end`.

Such a slicing expression works similar to the indexing expression, except
that it returns a new list holding the given range of items starting with the
one at index `start`, and stopping *right before* the exclusive index `end`.
This allows easy retrieval of sublists of a given list, or substrings of
strings or subparts of bytes objects.

If the start or end are omitted from the slicing expression (`:end` or
`start:`), `start` will be implicitly set to 0 and `end` will be implicitly
set to the full length of the list/string/bytes object respectively.

If the range resulting from the specified slicing expression is partially
or fully out of bounds of the sliced list/string/bytes object, only the items
that aren't out of bounds will be returned.
(so if the whole specified range is out of range, the result will simply be
an empty list/string/bytes object)

Usage example:
```tethercode
let numbers = [17, 32, 31, 64]
print("Numbers with first item ommitted: " +
    tostring(numbers[1:]))  # result: [32, 31, 64]
print("Middle two numbers: " + tostring(numbers[1:3]))  # result: [32, 31]
let name = "Johnson"
print("Shortened name: " + name[:4])  # result: "John"
```

### Operator precedence

Operators in tethercode like ```+``` or ```-``` which combine two elements
can be chained for multiple values similar to this: ```v1 + v2 * v3```

However, if multiple operators are mixed in such a way without brackets
indicating the order in which they are to be evaluated, the built-in
operator precedence is applied.

The operator precedence in tethercode behaves as follows (from the most closely
binding operator evaluated first to the most outer one), with similar-ranked
operators being evaluated left-to-right unless specified otherwise:

1. ```[..]```, ```.``` *(special: those operators are evaluated in the order
                         right-to-left)*
2. ```*```, ```/```
3. ```+```, ```-```
4. ```>=```, ```<=```, ```==```, ```!=```
5. ```in```
6. ```not```
7. ```and```
8. ```or```

## Scoping

Scoping in tethercode is done using a lexical scope at compile time. The
following rules is how an identifier can be resolved (which are checked
in that order):

- The identifier is one of the following:

  - the right-hand side of a member access operator, or
  - the name of a keyword argument in a function definition or
    function call, or
  - the name of a newly defined reference in a let statement, function
    definition or class definition

  In such a case, it doesn't itself refer to any previously defined
  independent element and it will not be resolved.

  The right-hand side of a member access operator *may be checked* by
  the compiler in some cases where this is feasible and if it refers
  to an item that is clearly never present at runtime it *may* generate
  a compiler error, but in general it is not guaranteed to be checked.

- In the `<code block>` where the current statement with the identifier
  is contained, there is a previous `<let statement>` where the 
  identifier is defined

- All parents of the current statement are evaluated from the most direct
  / inner parent to the outer ones:

  - The parent is a `<code block>` and has a '<let statement>' occuring
    before the child we just ascended from where the identifier is defined

  - The parent is a `<for loop>` and uses the identifier as a loop
    variable

  - The parent is a `<named function definition>` and the identifier is
    either the function name itself, or one of the function arguments
    (for keyword arguments it must be the left side to the `=` character)

- There is a 'let statement' as a top level statement somewhere in the
  same file

- There is an 'import statement' of the same name as the identifier.
  This is only valid if the identifier is the left-hand side of a member
  operator, because modules cannot be referenced directly themselves.
  (since they are no actual objects with a data type)

All the following rules above are evaluated fully at compile time, with
a compiler error resulting if an identifier cannot be resolved with
any of them.


