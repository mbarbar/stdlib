
# tethercode stdlib - url.tc
# Copyright (c) 2016,
# tethercode dev team (see AUTHORS.md / https://tethercode.io/go/authors )
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgement in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.
#
# SPDX-License-Identifier: Zlib

import encoding
import path
import platform

func _is_windows_drive_letter(c) {
    if type(c) == BaseType.bytes {
        c = encoding.decode(c, ignore_errors=true)
    }
    if c.length != 1 {
        return false
    }
    if (ord(c) >= ord("a") and ord(c) <= ord("z")) or
            (ord(c) >= ord("A") and ord(c) <= ord("Z")) {
        return true
    }
    return false
}

func _as_bytes(v) {
    if type(v) == BaseType.string {
        return encoding.encode(v, ignore_errors=true)
    }
    return v
}

class Url {
    let protocol
    let port
    let host
    let resource
    let user
    let password
    let is_remote

    func init(url_or_path,
            default_remote_protocol="ssh",
            prefer_remote=false,
            normalize=true,
            autoconvert_relative_path=true,
            as_bytes=false) {
        if type(url_or_path) == type(self) {
            self.protocol = url_or_path.protocol
            self.port = url_or_path.port
            self.host = url_or_path.host
            if type(self.host) == BaseType.string {
                self.host = encoding.encode(self.host,
                    ignore_errors=true)
            }
            self.resource = url_or_path.resource
            if type(self.resource) == BaseType.string {
                self.resource = encoding.encode(
                    self.resource, ignore_errors=true)
            }
            self.user = url_or_path.user
            if type(self.user) == BaseType.string {
                self.user = encoding.encode(
                    self.user, ignore_errors=true)
            }
            self.password = url_or_path.password
            if type(self.password) == BaseType.string {
                self.password = encoding.encode(
                    self.password, ignore_errors=true)
            }
            self.is_remote = url_or_path.is_remote
            if normalize {
                while self.resource.find(b"//") > 0 {
                    self.resource = self.resource.replace(b"/", b"//")
                }
                if not self.resource.startswith(b"/") {
                    self.resource = b"/" + self.resource
                }
                self.protocol = self.protocol.lower()
                if self.host != null {
                    self.host = self.host.lower()
                }
            }
            if not as_bytes {
                self.resource = encoding.decode(self.resource,
                    ignore_errors=true)
                if self.host != null {
                    self.host = encoding.decode(self.host,
                        ignore_errors=true)
                }
                if self.user != null {
                    self.user = encoding.decode(self.user,
                        ignore_errors=true)
                }
                if self.password != null {
                    self.password = encoding.decode(self.password,
                        ignore_errors=true)
                }
            }
            return
        }
        if type(url_or_path) == BaseType.string {
            url_or_path = encoding.encode(url_or_path, "utf-8")
        }
        if type(default_remote_protocol) == BaseType.bytes {
            default_remote_protocol = encoding.decode(
                default_remote_protocol, "utf-8")
        }

        # Examine whether there is a protocol specifying head:
        let has_protocol_head = false
        let i = 0
        while i < url_or_path.length {
            if url_or_path[i] == b"/" or url_or_path[i] == b"\\" {
                break
            }
            if url_or_path[i:].startswith(b"://") and i > 1 {
                has_protocol_head = true
                self.protocol = encoding.decode(
                    url_or_path[:i], "utf-8").lower()
                url_or_path = url_or_path[i + b"://".length:]
                break
            } elseif url_or_path[i] == b":" or url_or_path[i] == b"@" {
                break
            }
            i += 1
        }
        if self.protocol == null {
            # Check if this is a regular file path:
            i = 0
            while i < url_or_path.length {
                if url_or_path[i] == b"/" or url_or_path[i] == b"\\" {
                    # It appears to be a regular file path!
                    self.protocol = "file"
                } elseif (url_or_path[i] == b":" or url_or_path[i] == b"@")
                        and  # Make sure this isn't C:\..\ windows-style:
                        (url_or_path[i] != b":" or i != 1 or
                        not _is_windows_drive_letter(url_or_path[0]) or
                        (url_or_path.length > 2 and url_or_path[2] != b"/"
                        and url_or_path[2] != b"\\")) {
                    # This looks like an URL:
                    self.protocol = default_remote_protocol
                }
                i += 1
            }

            # Make sure we decide for a protocol at this point:
            if self.protocol == null {
                if prefer_remote {
                    self.protocol = default_remote_protocol
                } else {
                    self.protocol = "file"
                }
            }

            if self.protocol == "file" and
                    not url_or_path.startswith(b"/") {
                # Handle relative paths in a smart way:
                if autoconvert_relative_path and
                        not path.isabs(url_or_path) and
                        url_or_path.find(b"@") < 0 and
                        url_or_path.find(b":") < 0 {
                    # This is a relative path, import correctly as url:
                    url_or_path = path.abspath(url_or_path).replace(
                        path.separator_bytes(), b"/").replace(b"\\", b"/")
                    if not url_or_path.startswith(b"/") {
                        url_or_path = b"/" + url_or_path
                    }
                } else {
                    url_or_path = b"/" + url_or_path
                }
            }
        }

        # If normalizing, lower protocol and host:
        if normalize {
            self.protocol = self.protocol.lower()
            if self.host != null {
                self.host = self.host.lower()
            }
        }

        # Check whether this is obviously not remote:
        if self.protocol.lower() == "file" {
            self.is_remote = false
        }

        # Extract user if present:
        let end_of_user = -1
        let haspw = false
        i = 0
        while i < url_or_path.length {
            if url_or_path[i] == b"/" or url_or_path[i] == b"\\" {
                break
            }
            if url_or_path[i] == b"@" or url_or_path[i] == b":" {
                if url_or_path[i] == b":" {
                    # Given a :, make sure that @ follows:
                    if url_or_path[i:].find(b"@") <= 0 {
                        break
                    }
                    haspw = true
                }
                # Return @ or : position:
                end_of_user = i
                break
            }
            i += 1
        }
        if end_of_user > 0 {
            if self.protocol.lower() != "file" {
                self.is_remote = true
            }
            self.user = url_or_path[0:end_of_user]
            url_or_path = url_or_path[end_of_user + 1:]
            if haspw {
                let pw_end = url_or_path.find(b"@")
                self.password = url_or_path[:pw_end]
                url_or_path = url_or_path[pw_end + 1:]
            }
        }

        # Separate host and resource:
        let first_colon = url_or_path.find(b":")
        let first_slash = url_or_path.find(b"/")
        let host_end = first_colon
        if host_end < 0 or (first_slash >= 0 and
                first_slash < host_end) {
            host_end = first_slash
        }
        if host_end > 0 and self.protocol.lower() != "file" {
            self.host = url_or_path[:host_end]
            self.resource = url_or_path[host_end:].replace(
                path.separator_bytes(), b"/").replace(b"\\", b"/")
        } else {
            self.resource = url_or_path.replace(
                path.separator_bytes(), b"/").replace(b"\\", b"/")
        }
        if normalize {
            while self.resource.find(b"//") >= 0 {
                self.resource = self.resource.replace(b"//", b"/")
            }
        }
        if not self.resource.startswith(b"/") {
            self.resource = b"/" + self.resource
        }
        assert(self.resource.find(b"//") < 0)

        # Make sure host is valid:
        if self.host != null and self.host.startswith(b"-") {
            throw ValueException("invalid URL - " +
                "hostname cannot start with " +
                "a dash")
        }
        if self.host != null and (self.host.find(b"%") >= 0 or
                self.host.find(b"$") >= 0 or
                self.host.find(b"\n") >= 0 or
                self.host.find(b"\r") >= 0 or
                self.host.find(b"\\") >= 0 or
                self.host.find(b"\x00") >= 0) {
            throw ValueException("invalid URL - " +
                "hostname cannot contain the following characters: " +
                "%, $, '\\n', '\\r', \\, '\\x0'")
        }

        # Convert /c:/some/path (windows-style) to /c/some/path
        if self.protocol.lower() == "file" and
                self.resource.startswith(b"/") and
                self.resource.length >= 3 and
                _is_windows_drive_letter(self.resource[1]) and
                self.resource[2] == b":" and
                (self.resource.length < 4 or
                self.resource[3] == b"/") {
            self.resource = self.resource[:2] + self.resource[3:]
        }

        # If as_bytes=false, convert to unicode:
        if as_bytes == false {
            if self.host != null {
                self.host = encoding.decode(self.host,
                    ignore_errors=true)
            }
            self.resource = encoding.decode(self.resource,
                ignore_errors=true)
            if self.user != null {
                self.user = encoding.decode(self.user,
                    ignore_errors=true)
            }
            if self.password != null {
                self.password = encoding.decode(self.password,
                    ignore_errors=true)
            }
        }
    }

    func equals(other) {
        if type(other) != type(self) {
            return false
        }
        if (other.protocol != self.protocol and
                    (self.protocol == null or
                    other.protocol == null or
                    self.protocol.lower() != other.protocol.lower()
                    )) or
                (other.host != self.host and
                    (self.host == null or 
                    other.host == null or 
                    self.host.lower() != other.host.lower()
                    )) or
                other.port != self.port or
                other.user != self.user or
                other.password != self.password or
                path.normalize(other.resource) != path.normalize(
                    self.resource) {
            return false
        }
        return true
    }

    func local_disk_path {
        let result
        if type(self.resource) == BaseType.bytes {
            result = path.normalize(self.resource).replace(
                b"/", path.separator_bytes())
        } else {
            result = path.normalize(self.resource).replace(
                "/", path.separator())
        }
        return result
    }

    func urlstr(as_bytes=false) {
        let t = encoding.encode(self.protocol) + b"://"
        if self.host != null {
            if self.user != null {
                if self.password != null {
                    t += _as_bytes(self.user) + b":" +
                        _as_bytes(self.password) + b"@"
                } else {
                    t += _as_bytes(self.user) + b"@"
                }
            }
            t += _as_bytes(self.host)
            if self.port != null {
                t += b":" + _as_bytes(tostring(self.port))
             }
        }
        let resource = _as_bytes(self.resource)
        if self.protocol == "git" and self.port == null and
                self.host != null {
            t += b":"
            if resource.startswith(b"/") {
                t += resource[path.separator_bytes().length:]
            } else {
                t += resource
            }
        } else {
            if not resource.startswith(path.separator_bytes()) {
                t += b"/"
            }
            t += resource
        }
        if as_bytes {
            return t
        }
        return encoding.decode(t, ignore_errors=true)
    }

    func tostring {
        return "<Url(\"" + self.urlstr().replace(
            "\\", "\\\\").replace(
            "\"", "\\\"") + "\")>"
    }
}

